/**
 * todo 流水线
 * @date 2024/1/5 - 17:26:27
 * @author Holy-Mortal
 */

/**
 * 需求
 *    一个工厂有m条流水线，来并行完成n个独立的作业，该工厂设置了一个调度系统，
 *    在安排作业时，总是优先执行处理时间最短的作业。
 *    现给定流水线个数m，需要完成的作业数n, 每个作业的处理时间分别为t1,t2…tn。
 *    请你编程计算处理完所有作业的耗时为多少？
 *    当n>m时，首先处理时间短的m个作业进入流水线，其他的等待，
 *    当某个作业完成时，依次从剩余作业中取处理时间最短的进入处理。
 * 输入描述
 *    第一个参数：流水线个数，取值范围 (0, 100)
 *    第二个参数：作业总数，取值范围 (0, 100)
 *    第三个参数：每个作业处理时长得到的数组，每项取值范围 (0, 100)
 * 输出描述
 *    输出处理完所有作业的总时长
 * 示例
 *    输入: (3, 5, [8, 4, 3, 2, 10]) => 输出: 13
 *    说明
 *        1、先安排时间为2、3、4的3个作业。
 *        2、第一条流水线先完成作业，然后调度剩余时间最短的作业8。
 *        3、第二条流水线完成作业，然后调度剩余时间最短的作业10。
 *        4、总工耗时就是第二条流水线完成作业的时间13（3+10）。
 */
const handlePipeLine = (pipe, total, arr) => {
  /** 校验输入参数 */
  if (!determineInteger(pipe)) {
    return console.error("pipe 必须是一个整数！");
  }
  if (pipe <= 0 || pipe >= 100) {
    return console.error("pipe 取值范围 (0, 100)");
  }
  if (!determineInteger(total)) {
    return console.error("total 必须是一个整数！");
  }
  if (total <= 0 || total >= 100) {
    return console.error("total 取值范围 (0, 100)");
  }
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  if (total !== arr.length) {
    return console.error("total 必须和 arr 长度保持一致！");
  }
  const flag = arr.every(
    (item) => determineInteger(item) && item > 0 && item < 100
  );
  if (!flag) {
    return console.error("arr 中每一项为(0,100)内的整数！");
  }

  /** 核心代码区域 */
  arr.sort((a, b) => a - b);
  let result = new Array(pipe).fill(0);
  for (let i = 0; i < arr.length; i++) {
    result.sort((a, b) => a - b);
    result[0] += arr[i];
  }
  return result.sort((a, b) => b - a)[0];
};

console.log("test: ", handlePipeLine(3, 5, [8, 4, 3, 2, 10]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
