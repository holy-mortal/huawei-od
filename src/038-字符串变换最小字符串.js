/**
 * todo 字符串变换最小字符串
 * @date 2024/1/2 - 10:19:04
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定一个字符串s，最多只能进行一次变换，返回变换后能得到的最小字符串（按照字典序进行比较）
 *    变换规则：交换字符串中任意两个不同位置的字符
 * 输入描述
 *    一串小写字母组成的字符串s
 * 输出描述
 *    按照要求进行变换得到的最小字符串
 * 示例
 *    输入: ("abcdef") => 输出: "abcdef", abcdef已经是最小字符串，不需要交换
 *    输入: ("bcdefa") => 输出: "acdefb", a和b进行位置交换，可以等到最小字符串
 * 备注
 *    s是都是小写字符组成，1 <= s.length <= 1000
 */
const handleStringToMin = (str) => {
  /** 校验输入参数 */
  if (!str && str !== "") {
    return console.error("参数异常，请核实！");
  }
  if (str.length < 1 || str.length > 100) {
    return console.error("str 长度不超过 100");
  }
  const rule = /^[a-z]+$/;
  if (!rule.test(str)) {
    return console.error("str 只包含小写字母！");
  }

  /** 核心代码区域 */
  let arr = str.split("").reverse();
  for (let i = "a".charCodeAt(0); i <= "z".charCodeAt(0); i++) {
    let index = arr.findIndex((item) => item === String.fromCharCode(i));
    if (index !== -1) {
      let temp = arr[arr.length - 1];
      arr[arr.length - 1] = arr[index];
      arr[index] = temp;
      break;
    }
  }
  return arr.reverse().join("");
};

console.log("test1: ", handleStringToMin("abcdef"));
console.log("test2: ", handleStringToMin("bcdefa"));
