/**
 * todo 找终点
 * @date 2024/1/5 - 17:53:33
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定一个正整数数组，设为nums，最大为100个成员，
 *    求从第一个成员开始，正好走到数组最后一个成员，所使用的最少步骤数。
 * 要求：
 *    1、第一步必须从第一元素开始，且1<=第一步的步长<len/2;（len为数组的长度，需要自行解析）。
 *    2、从第二步开始，只能以所在成员的数字走相应的步数，不能多也不能少, 如果目标不可达返回-1，只输出最少的步骤数量
 *    3、只能向数组的尾部走，不能往回走。
 * 输入描述
 *    正整数数组，长度范围 [0, 100]
 * 输出描述
 *    正整数，表示最少的步数，如果不存在输出-1
 * 示例
 *    输入: (7, 5, 9, 4, 2, 6, 8, 3, 5, 4, 3, 9) => 输出: 2, 第一步： 第一个可选步长选择2，从第一个成员7开始走2步，到达9；第二步： 从9开始，经过自身数字9对应的9个成员到最后。
 *    输入: (1, 2, 3, 7, 1, 5, 9, 3, 2, 1) => 输出: -1
 */
const handleFindDestination = (arr) => {
  /** 校验输入参数 */
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  if (arr.length < 0 || arr.length > 100) {
    return console.error("arr 长度范围[0, 100]");
  }
  const flag = arr.every((item) => determineInteger(item) && item > 0);
  if (!flag) {
    return console.error("arr 每一项都为正整数");
  }

  /** 核心代码区域 */
  const temp = [];
  for (let i = 1; i < Math.floor(arr.length / 2); i++) {
    let step = [];
    let count = i;
    while (count <= arr.length) {
      step.push(count);
      count = count + arr[count];
    }
    if (step[step.length - 1] === arr.length - 1) {
      temp.push(step);
    }
  }
  if (temp.length === 0) {
    return -1;
  }
  const map = new Map();
  for (let i = 0; i < temp.length; i++) {
    const item = temp[i];
    const key = item.length;
    if (map.has(key)) {
      let arr = map.get(key);
      arr.push(item);
      map.set(key, arr);
    } else {
      map.set(key, [item]);
    }
  }
  const result = Array.from(map).sort((a, b) => a[0] - b[0])[0];
  return {
    step: result[0],
    path: result[1],
  };
};

console.log(
  "test1: ",
  handleFindDestination([7, 5, 9, 4, 2, 6, 8, 3, 5, 4, 3, 9])
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
