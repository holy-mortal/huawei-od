/**
 * todo 矩形相交面积
 * @date 2023/12/26 - 19:30:33
 * @author Holy-Mortal
 */

/**
 * 需求
 *    在坐标系中，给定3个矩形，求相交区域的面积
 * 输入描述
 *    输入3个矩形的位置，分别代表
 *    ‘左上角x坐标’，‘左上角y坐标’，‘矩形宽’，‘矩形高’
 *    -1000 <= x,y < 1000
 * 输出描述
 *    输出3个矩形相交的面积，不相交的输出0
 * 示例
 *    输入: ([[1, 6, 4, 4], [3, 5, 3, 4], [0, 3, 7, 3]]) => 输出: 2
 *    A：左上角坐标(1, 6)，宽4，高4
 *    B：左上角坐标(3, 5)，宽3，高4
 *    C：左上角坐标(0, 3)，宽7，高3
 */
const handleRectIntersectionArea = (matrix) => {
  /** 校验输入参数 */
  if (!Array.isArray(matrix)) {
    return console.error("matrix 必须是一个数组");
  }
  const flag = matrix.every((item) => {
    return (
      item.every((it) => determineInteger(it)) &&
      item[0] >= -1000 &&
      item[0] <= 1000 &&
      item[1] >= -1000 &&
      item[1] <= 1000 &&
      item[2] >= 0 &&
      item[3] >= 0
    );
  });
  if (!flag) {
    return console.error(
      "matrix 必须是一个数组，且每个元素必须是长度为4的整数数组"
    );
  }

  /** 核心代码区域 */
  const arr = [];
  for (let a = 0; a < matrix.length; a++) {
    const temp = [];
    for (let b = 0; b <= matrix[a][2]; b++) {
      for (let c = 0; c <= matrix[a][3]; c++) {
        const x = matrix[a][0];
        const y = matrix[a][1];
        temp.push(`${x + b},${y - c}`);
      }
    }
    arr.push(temp);
  }
  let result = arr[0];
  for (let i = 1; i < arr.length; i++) {
    result = result.filter((item) => arr[i].includes(item));
  }
  if (result.length === 0) {
    return 0;
  }
  result = result
    .map((item) => {
      const it = item.split(",");
      return [Number(it[0]), Number(it[1])];
    })
    .sort((a, b) => {
      if (a[0] === b[0]) {
        return a[1] - b[1];
      }
      return a[0] - b[0];
    });
  return (
    (result[result.length - 1][1] - result[0][1]) *
      result[result.length - 1][0] -
    result[0][0]
  );
};

console.log(
  "test: ",
  handleRectIntersectionArea([
    [1, 6, 4, 4],
    [3, 5, 3, 4],
    [0, 3, 7, 3],
  ])
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
