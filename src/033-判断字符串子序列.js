/**
 * todo 判断字符串子序列
 * @date 2023/12/30 - 18:39:09
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定字符串 target和 source, 判断 target 是否为 source 的子序列
 *    你可以认为 target 和  source 中仅包含英文小写字母。
 *    字符串 source可能会很长（长度 ~= 500,000），而 target 是个短字符串（长度 <=100）。
 *    字符串的一个子序列是原始字符串删除一些（也可以不删除）字符而不改变剩余字符相对位置形成的新字符串。
 *    （例如，"abc"是"aebycd"的一个子序列，而"ayb"不是）。
 *    请找出最后一个子序列的起始位置。
 * 输入描述
 *    第一个参数，target 长度 <= 100
 *    第二个参数，source 长度 <= 500000
 * 输出描述
 *    最后一个子序列的起始位置， 即最后一个子序列首字母的下标
 * 示例
 *    输入: ("abc", "abcaybec") => 输出: 3
 */
const handleStringChildrenIndex = (target, source) => {
  /** 校验输入参数 */
  if (typeof target !== "string" || typeof source !== "string") {
    throw new Error("参数必须是字符串");
  }
  if (target.length <= 0 || target.length > 200) {
    return console.error("target 长度不超过 100");
  }
  if (source.length <= 0 || source.length > 500000) {
    return console.error("source 长度不超过 500000");
  }
  const rule = /[a-zA-Z]+/;
  if (!rule.test(target) || !rule.test(source)) {
    return console.error("字符串中只包含大小写字母！");
  }

  /** 核心代码区域 */
  let index = 0;
  let result = new Set();
  let temp = [];
  for (let i = 0; i < source.length; i++) {
    if (source[i] === target[index]) {
      temp[index] = i;
      index++;
      result.add(temp);
      if (index === target.length) {
        index = 0;
        temp = [];
      }
    }
  }
  result = Array.from(result);
  console.log("result: ", result);
  return result.length === 0 ? -1 : result[result.length - 1][0];
};

console.log("test1: ", handleStringChildrenIndex("abc", "aebycdab"));
console.log("test2: ", handleStringChildrenIndex("abc", "abcaybec"));
