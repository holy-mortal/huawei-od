/**
 * todo 求字符串中所有整数的最小值
 * @date 2024/1/2 - 13:07:51
 * @author Holy-Mortal
 */

/**
 * 需求
 *    输入字符串s，输出s中包含所有整数的最小和
 *    说明
 *    1. 字符串s，只包含 a-z A-Z +- ；
 *    2. 合法的整数包括
 *        1）正整数 一个或者多个0-9组成，如 0 2 3 002 102
 *        2）负整数 负号 - 开头，数字部分由一个或者多个0-9组成，如 -0 -012 -23 -00023
 * 输入描述
 *    包含数字的字符串
 * 输出描述
 *    所有整数的最小和
 * 示例
 *    输入: ("bb1234aa") => 输出: 10
 *    输入: ("bb12-34aa") => 输出: -31, 1+2+（-34） = 31
 */
const handleAllIntegerMinSum = (str) => {
  /** 校验输入参数 */
  if (str === undefined) {
    return console.error("str 必须存在！");
  }
  const rule = /[0-9a-zA-Z\-]+/;
  if (!rule.test(str)) {
    return console.error("str 满足格式要求");
  }

  /** 核心代码区域 */
  let arr = str.match(/[0-9\-]+/g);
  let result = 0;
  for (let i = 0; i < arr.length; i++) {
    let newStr = arr[i];
    if (newStr.includes("-")) {
      let newArr = newStr.split("-");
      for (let j = 0; j < newArr[0].length; j++) {
        result += Number(newArr[0][j]);
      }
      for (let j = 1; j < newArr.length; j++) {
        result -= Number(newArr[j]);
      }
    } else {
      for (let j = 0; j < newStr.length; j++) {
        result += Number(newStr[j]);
      }
    }
  }
  return result;
};

console.log("test1: ", handleAllIntegerMinSum("bb1234aa"));
console.log("test2: ", handleAllIntegerMinSum("bb12-34aa"));
