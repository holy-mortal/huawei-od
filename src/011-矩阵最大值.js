/**
 * todo 矩阵最大值
 * @date 2023/12/20 - 10:51:40
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定一个仅包含0和1的N*N二维矩阵，请计算二维矩阵的最大值
 *    计算规则如下：
 *        1、 每行元素按下标顺序组成一个二进制数（下标越大越排在低位），二进制数的值就是该行的值
 *            矩阵各行值之和为矩阵的值
 *        2、 允许通过向左或向右整体循环移动每行元素来改变各元素在行中的位置
 *            如： [1,0,1,1,1]向右整体循环移动2位变为[1,1,1,0,1]，二进制数为11101，值为29
 *            如： [1,0,1,1,1]向左整体循环移动2位变为[1,1,1,1,0]，二进制数为11110，值为30
 * 输入描述
 *    第一个参数为正整数，表示N的大小，范围(0,20]
 *    第二个参数为一个N阶矩阵，每个元素只能为0或1
 * 输出描述
 *    矩阵的最大值
 * 示例
 *    输入: (5, [[1,0,0,0,1],[0,0,0,1,1],[0,1,0,1,0],[1,0,0,1,1],[1,0,1,0,1]]) => 输出: 122
 *    第一行向右整体循环移动1位，得到本行的最大值[1,1,0,0,0]，二进制值为11000，十进制值为24。
 *    第二行向右整体循环移动2位，得到本行的最大值[1,1,0,0,0]，二进制值为11000，十进制值为24。
 *    第三行向左整体循环移动1位，得到本行的最大值[1,0,1,0,0]，二进制值为10100，十进制值为20。
 *    第四行向右整体循环移动2位，得到本行的最大值[1,1,1,0,0]，二进制值为11100，十进制值为28。
 *    第五行向右整体循环移动1位，得到本行的最大值[1,1,0,1,0]，二进制值为11010，十进制值为26。
 */
const handleMatrixMaxValue = (count, matrixArr) => {
  /** 校验输入参数 */
  if (!count || !matrixArr) {
    return console.error("参数缺失，请核实！");
  }
  if (!determineInteger(count)) {
    return console.error("count 必须是一个整数！");
  }
  if (count <= 0 || count > 20) {
    return console.error("count 取值范围为 (0,20]");
  }
  if (!Array.isArray(matrixArr)) {
    return console.error("matrixArr 必须是一个数组");
  }
  if (count !== matrixArr.length) {
    return console.error("matrixArr 的长度必须与 count 相等");
  }
  const flag1 = matrixArr.every((item) => Array.isArray(item));
  if (!flag1) {
    return console.error("matrixArr 必须是一个二维数组");
  }
  const flag2 = matrixArr.every((item) => {
    return item.every((it) => {
      return (
        typeof it === "number" && !Number.isNaN(it) && (it === 0 || it === 1)
      );
    });
  });
  if (!flag2) {
    return console.error("matrixArr 必须是一个二维数组，且每个元素只能为0或1");
  }

  /** 核心代码区域 */
  let result = 0;
  for (let i = 0; i < count; i++) {
    const arr = matrixArr[i];
    let maxRow = 0;
    for (let j = 0; j < arr.length; j++) {
      const last = arr.pop();
      arr.unshift(last);
      maxRow = Math.max(parseInt(arr.join(""), 2), maxRow);
    }
    result += maxRow;
  }
  return result;
};

console.log(
  "test: ",
  handleMatrixMaxValue(5, [
    [1, 0, 0, 0, 1],
    [0, 0, 0, 1, 1],
    [0, 1, 0, 1, 0],
    [1, 0, 0, 1, 1],
    [1, 0, 1, 0, 1],
  ])
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
// const handleMatrixMaxValue = (n, matrix) => {
//   let maxSum = 0;
//   for (let i = 0; i < n; i++) {
//     const binaryStr = matrix[i]
//       .map((item, index) => item * Math.pow(2, index))
//       .join("");
//     maxSum += parseInt(binaryStr);
//   }
//   return maxSum;
// };
