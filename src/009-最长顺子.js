/**
 * todo 最长顺子
 * @date 2023/12/19 - 18:01:27
 * @author Holy-Mortal
 */

/**
 * 需求
 *    斗地主起源于湖北十堰房县，据传是一位叫吴修全的年轻人根据当地流行的扑克玩法“跑得快”改编的
 *    牌型：单顺, 又称顺子，最少5张牌，最多12张牌（3⋯A），不能有2，也不能有大小王，不计花色
 *        例如：3-4-5-6-7-8，7-8-9-10-J-Q，3-4-5-6-7-8-9-10-J-Q-K-A
 *    可用的牌 3<4<5<6<7<8<9<10<J<Q<K<A<2 < B(小王)< C(大王
 *    每种牌除大小王外有4种花色(共有 13X4 + 2 张牌)
 *    输入1. 手上已有的牌 2. 已经出过的牌（包括对手出的和自己出的牌）
 *    输出: 对手可能构成的最长的顺子(如果有相同长度的顺子, 输出牌面最大的那一个)
 *    如果无法构成顺子, 则输出 NO-CHAIN
 * 输入描述
 *    输入的第一行为当前手中的牌，输入的第二行为已经出过的牌
 * 输出描述
 *    最长的顺子
 * 示例
 *    输入: ("3-3-3-3-4-4-5-5-6-7-8-9-10-J-Q-K-A", "4-5-6-7-8-8-8") => 输出: "9-10-J-Q-K-A"
 *    输入: ("3-3-3-3-8-8-8-8", "K-K-K-K") => 输出: "NO-CHAIN"
 */
const handleMaxLengthStraight = (selfStr, existStr) => {
  /** 校验输入参数 */
  if (!selfStr) {
    return console.error("selfStr 不能为空!");
  }
  const rules =
    /^(3|4|5|6|7|8|9|10|J|Q|K|A|2|B|C)(-(3|4|5|6|7|8|9|10|J|Q|K|A|2|B|C))+$/;
  if (!rules.test(selfStr)) {
    return console.error("selfStr 不符合格式！");
  }
  if (existStr && !rules.test(existStr)) {
    return console.error("existStr 不符合格式！");
  }

  /** 核心代码区域 */
  const map = new Map();
  const quadrille = [
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "J",
    "Q",
    "K",
    "A",
    "2",
  ];
  quadrille.forEach((item) => map.set(item, 4));
  map.set("B", 1);
  map.set("C", 1);

  selfStr.split("-").forEach((item) => {
    if (map.has(item)) {
      map.set(item, map.get(item) - 1);
    }
  });
  existStr.split("-").forEach((item) => {
    if (map.has(item)) {
      map.set(item, map.get(item) - 1);
    }
  });
  map.delete("2");
  map.delete("B");
  map.delete("C");
  const arr = Array.from(map);
  const newArr = arr.map((item) => {
    return item[1] === 0 ? 0 : 1;
  });
  const { startIndex, endIndex } = getLongestSequenceIndexes(newArr);
  const resArr = [];
  if (endIndex - startIndex >= 4) {
    for (let i = startIndex; i <= endIndex; i++) {
      resArr.push(arr[i][0]);
    }
    return resArr.join("-");
  } else {
    return "NO-CHAIN";
  }
};

console.log("test1: ", handleMaxLengthStraight("3-3-3-3-8-8-8-8", "K-K-K-K"));
console.log(
  "test2: ",
  handleMaxLengthStraight("3-3-3-3-4-4-5-5-6-7-8-9-10-J-Q-K-A", "4-5-6-7-8-8-8")
);

/**
 * 最长子序列
 */
function getLongestSequenceIndexes(arr) {
  let maxStart = 0;
  let maxLength = 0;
  let currLength = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === 1) {
      currLength++;
    } else {
      if (currLength > maxLength) {
        maxLength = currLength;
        maxStart = i - maxLength;
      }
      currLength = 0;
    }
  }
  if (currLength > maxLength) {
    maxLength = currLength;
    maxStart = arr.length - maxLength;
  }
  const maxEnd = maxStart + maxLength - 1;
  return { startIndex: maxStart, endIndex: maxEnd };
}
