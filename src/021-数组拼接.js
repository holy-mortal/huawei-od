/**
 * todo 数组拼接
 * @date 2023/12/28 - 16:18:56
 * @author Holy-Mortal
 */

/**
 * 需求
 *    现在有多组整数数组，需要将它们合并成一个新的数组。合并规则
 *    从每个数组里按顺序取出固定长度的内容合并到新的数组中，取完的内容会删除掉
 *    如果该行不足固定长度或者已经为空，则直接取出剩余部分的内容放到新的数组中，继续下一行
 * 输入描述
 *    第一个参数，每次读取的固定长度，取值范围(0, 10)
 *    第二个参数，整数数组的数目，取值范围(0, 1000)
 *    第三个参数, 需要合并的数组，每个数组长度不超过 100
 * 输出描述
 *    输出一个新的数组
 * 示例
 *    输入: (3, 2, [[2, 5, 6, 7, 9, 5, 7], [1, 7, 4, 3, 4]]) => 输出: [2, 5, 6, 1, 7, 4, 7, 9, 5, 3, 4, 7]
 *    输入: (4, 3, [[1, 2, 3, 4, 5, 6], [1, 2, 3], [1, 2, 3, 4]]) => 输出: [1, 2, 3, 4, 1, 2, 3, 1, 2, 3, 4, 5, 6]
 */
const handleArrContact = (readLen, count, matrix) => {
  /** 校验输入参数 */
  if (!readLen || !count || !matrix) {
    return console.error("参数缺失，请核实！");
  }
  if (!determineInteger(readLen)) {
    return console.error("readLen 必须是一个整数！");
  }
  if (readLen <= 0 || readLen >= 10) {
    return console.error("readlen 取值范围必须是(0, 10)");
  }
  if (!determineInteger(count)) {
    return console.error("count 必须是一个整数！");
  }
  if (count <= 0 || count >= 1000) {
    return console.error("count 取值范围必须是(0, 1000)");
  }
  if (!Array.isArray(matrix)) {
    return console.error("matrix 必须是一个数组！");
  }
  if (count !== matrix.length) {
    return console.error("count 与 matrix 长度保持一致！");
  }
  const flag = matrix.every((item) => {
    return (
      Array.isArray(item) &&
      item.length <= 100 &&
      item.every((it) => {
        return determineInteger(it);
      })
    );
  });
  if (!flag) {
    return console.error("matrix 长度不超过 100，且每一项都是数字！");
  }

  /** 核心代码区域 */
  let i = 0;
  let result = [];
  while (matrix.join("").length > 0) {
    result = result.concat(matrix[i].splice(0, readLen));
    i++;
    if (i === count) i = 0;
  }
  return result;
};

console.log(
  "test1: ",
  handleArrContact(3, 2, [
    [2, 5, 6, 7, 9, 5, 7],
    [1, 7, 4, 3, 4],
  ])
);
console.log(
  "test2: ",
  handleArrContact(4, 3, [
    [1, 2, 3, 4, 5, 6],
    [1, 2, 3],
    [1, 2, 3, 4],
  ])
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
