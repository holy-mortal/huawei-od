/**
 * todo 最远足迹
 * @date 2024/1/4 - 09:35:31
 * @author Holy-Mortal
 */

/**
 * 需求
 *    某探险队负责对地下洞穴进行探险。探险队成员在进行探险任务时，
 *    随身携带的记录器会不定期地记录自身的坐标，但在记录的间隙中也会记录其他数据。
 *    探索工作结束后，探险队需要获取到某成员在探险过程中相对于探险队总部的最远的足迹位置
 *    1. 仪器记录坐标时，坐标的数据格式为(x,y)，如(1,2)、(100,200)，其中0<x<1000，0<y<1000。
 *       同时存在非法坐标，如(01,1)、(1,01)，(0,100)属于非法坐标。
 *    2. 设定探险队总部的坐标为(0,0)，某位置相对总部的距离为：x*x+y*y。
 *    3. 若两个座标的相对总部的距离相同，则第一次到达的坐标为最远的足迹。
 *    4. 若记录仪中的坐标都不合法，输出总部坐标（0,0）。
 * 备注：
 *    不需要考虑双层括号嵌套的情况，比如sfsdfsd((1,2))
 * 输入描述
 *    字符串，表示记录仪中的数据。如：ferga13fdsf3(100,200)f2r3rfasf(300,400)
 * 输出描述
 *    字符串，表示最远足迹到达的坐标。如： (300,400)
 * 示例
 *    输入: ("ferg(3,10)a13fdsf3(3,4)f2r3rfasf(5,10)") => 输出: (5, 10), 记录仪中的合法坐标有3个： (3,10)， (3,4)， (5,10)，其中(5,10)是相距总部最远的坐标， 输出(5,10)
 *    输入: ("asfefaweawfaw(0,1)fe") => 输出: (0, 0), 记录仪中的坐标都不合法，输出总部坐标（0,0）
 */
const handleMaxDistance = (str) => {
  /** 校验输入参数 */
  if (typeof str !== "string") {
    return console.error("str 必须是一个字符串！");
  }

  /** 核心代码区域 */
  let arr = str.match(/(\d+,\d+)/g);
  if (arr.length === 0) {
    return "(0, 0)";
  }
  arr = arr.map((item) => item.split(","));
  const flag = arr.every((item) => {
    return (
      Number(item[0]).toString() === item[0] &&
      Number(item[1]).toString() === item[1] &&
      Number(item[0]) !== 0
    );
  });
  if (!flag) {
    return "(0, 0)";
  }
  const map = new Map();
  for (let i = 0; i < arr.length; i++) {
    const item = arr[i];
    const key = Math.pow(Number(item[0]), 2) + Math.pow(Number(item[1]), 2);
    if (map.has(key)) {
      let temp = map.get(key);
      temp.push(`(${item[0]}, ${item[1]})`);
      map.set(key, temp);
    } else {
      map.set(key, [`(${item[0]}, ${item[1]})`]);
    }
  }
  const result = Array.from(map).sort((a, b) => b[0] - a[0]);
  return result[0];
};

console.log(
  "test1: ",
  handleMaxDistance("ferg(3,10)a13fdsf3(3,4)f2r3rfasf(5,10)")
);
console.log("test2: ", handleMaxDistance("asfefaweawfaw(0,1)fe"));
