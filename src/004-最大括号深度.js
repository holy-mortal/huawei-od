/**
 * todo 最大括号深度
 * @date 2023/12/18 - 15:16:01
 * @author Holy-Mortal
 */

/**
 * 需求
 *    现有一字符串仅由 '('，')'，'{'，'}'，'['，']'六种括号组成
 *    若字符串满足以下条件之一，则为无效字符串：
 *      1. 任一类型的左右括号数量不相等；
 *      2. 存在未按正确顺序（先左后右）闭合的括号。
 *    输出括号的最大嵌套深度，若字符串无效则输出0。
 *    0 ≤ 字符串长度 ≤ 100000
 * 输入描述
 *    一个只包括 '('，')'，'{'，'}'，'['，']'的字符串
 * 输出描述
 *    一个整数，最大的括号深度
 * 示例
 *    输入: ("[]") => 输出: 1, 有效字符串，最大嵌套深度为1
 *    输入: ("([]{()})") => 输出: 3, 有效字符串，最大嵌套深度为3
 *    输入: ("(]") => 输出: 0, 无效字符串，有两种类型的左右括号数量不相等
 *    输入: ("([)]") => 输出: 0, 无效字符串，存在未按正确顺序闭合的括号
 *    输入: (")(") => 输出: 0, 无效字符串，存在未按正确顺序闭合的括号
 */
const handleMaxBracketDepth = (str) => {
  /** 校验输入参数 */
  if (!str || typeof str !== "string") {
    return console.error("参数缺失或者参数非字符串类型！");
  }
  if (str.length > 100000 || str.length < 0) {
    return console.error("输入的字符串长度范围：0 ≤ 字符串长度 ≤ 100000");
  }
  const rules = /^[()\[\]{}]+$/;
  if (!rules.test(str)) {
    return console.error("字符串必须仅包含'('，')'，'{'，'}'，'['，']'！");
  }

  /** 核心代码区域 */
  const stack = [];
  let maxDepth = 0;
  for (let i = 0; i < str.length; i++) {
    const char = str[i];
    if (["(", "[", "{"].includes(char)) {
      stack.push(char);
      console.log(1, stack);
    } else if ([")", "]", "}"].includes(char)) {
      // 栈中如果不存在元素，但当前是反括号，则不能按顺序闭合
      if (stack.length === 0) {
        return 0;
      }
      // 取出栈顶的元素
      const top = stack.pop();
      if (["(", "[", "{"].indexOf(top) !== [")", "]", "}"].indexOf(char)) {
        return 0;
      }
    }
    maxDepth = Math.max(maxDepth, stack.length);
  }
  return maxDepth;
};

console.log("test1: ", handleMaxBracketDepth("[]")); // 1
console.log("test2: ", handleMaxBracketDepth("([]{()})")); // 3
console.log("test3: ", handleMaxBracketDepth("(]")); // 0
console.log("test4: ", handleMaxBracketDepth("([)]")); // 0
console.log("test5: ", handleMaxBracketDepth(")(")); // 0
