/**
 * todo 太阳能板最大面积
 * @date 2024/1/4 - 12:43:16
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给航天器一侧加装长方形或正方形的太阳能板（图中的红色斜线区域），需要先安装两个支柱（图中的黑色竖条），
 *    再在支柱的中间部分固定太阳能板。但航天器不同位置的支柱长度不同，
 *    太阳能板的安装面积受限于最短一侧的那根支柱长度。
 *    如图：
 *    现提供一组整形数组的支柱高度数据，假设每根支柱间距离相等为1个单位长度，计算如何选择两根支柱可以使太阳能板的面积最大。
 * 输入描述
 *    10,9,8,7,6,5,4,3,2,1
 *    注：支柱至少有2根，最多10000根，能支持的高度范围1~10^9的整数。柱子的高度是无序的，例子中递减只是巧合
 * 输出描述
 *    可以支持的最大太阳能板面积
 * 示例
 *    输入: ([10,9,8,7,6,5,4,3,2,1]) => 输出: 25, 备注: 10米高支柱和5米高支柱之间宽度为5，高度取小的支柱高也是5，面积为25。任取其他两根支柱所能获得的面积都小于25。所以最大的太阳能板面积为25
 */
const handleMaxArea = (arr) => {
  /** 校验输入参数 */
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  if (arr.length < 2 || arr.length > 10000) {
    return console.error("arr 长度范围[2, 10000]");
  }
  const flag = arr.every((item) => {
    return determineInteger(item) && item >= 1 && item <= Math.pow(9, 10);
  });
  if (!flag) {
    return console.error("数组中每一项取值范围[1, 1000000000]");
  }
};

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
