/**
 * todo 判断一组不等式是否满足约束并输出最大值
 * @date 2023/12/30 - 16:01:30
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定一组不等式，判断是否成立并输出不等式的最大差(输出浮点数的整数部分)
 *    要求：
 *        1）不等式系数为double类型，是一个二维数组；
 *        2）不等式的变量为int类型，是一维数组；
 *        3）不等式的目标值为double类型，是一维数组；
 *        4）不等式约束为字符串数组，只能是：">",">=","<","<=","="
 *    例如：
 *        不等式组：
 *            a11 * x1 + a12 * x2 + a13 * x3 + a14 * x4 + a15 * x5 <= b1;
 *            a21 * x1 + a22 * x2 + a23 * x3 + a24 * x4 + a25 * x5 <= b2;
 *            a31 * x1 + a32 * x2 + a33 * x3 + a34 * x4 + a35 * x5 <= b3;
 *    最大差值 = max {
 *        (a11 * x1 + a12 * x2 + a13 * x3 + a14 * x4 + a15 * x5 - b1),
 *        (a21 * x1 + a22 * x2 + a23 * x3 + a24 * x4 + a25 * x5 - b2),
 *        (a31 * x1 + a32 * x2 + a33 * x3 + a34 * x4 + a35 * x5 - b3)
 *    }, 类型为整数(输出浮点数的整数部分)
 * 输入描述
 *    1）不等式组系数(double类型)：
 *        a11,a12,a13,a14,a15
 *        a21,a22,a23,a24,a25
 *        a31,a32,a33,a34,a35
 *    2）不等式变量(int类型)：x1,x2,x3,x4,x5
 *    3）不等式目标值(double类型)：b1,b2,b3
 *    4) 不等式约束(字符串类型): <=,<=,<=
 * 输出描述
 *    true 或者 false, 最大差
 * 示例
 *    输入: ([[2.3,3,5.6,7,6],[11,3,8.6,25,1],[0.3,9,5.3,66,7.8]],[1,3,2,7,5],[340,670,80.6],[<=,<=,<=]) => 输出: false 458
 *    输入: ([[2.36,3,6,7.1,6],[1,30,8.6,2.5,21],[0.3,69,5.3,6.6,7.8]],[1,13,2,17,5],[340,67,300.6],[<=,>=,<=]) => 输出: false 458
 */
const handleInequation = (matirx, xArr, bArr, signArr) => {
  /** 校验输入参数 */
  if (
    !Array.isArray(matirx) ||
    !Array.isArray(xArr) ||
    !Array.isArray(bArr) ||
    !Array.isArray(signArr)
  ) {
    return console.error("每个参数均为数组！");
  }

  /** 核心代码区域 */
  const temp = [];
  for (let i = 0; i < matirx.length; i++) {
    let sum = 0;
    for (let j = 0; j < xArr.length; j++) {
      sum += matirx[i][j] * xArr[j];
    }
    temp.push({
      index: i,
      value: sum - bArr[i],
    });
  }
  const resObj = temp.sort((a, b) => {
    return Math.abs(b.value) - Math.abs(a.value);
  })[0];
  let result = [false, resObj.value];
  switch (signArr[resObj.index]) {
    case "<":
      result[0] = result[1] < 0;
      break;
    case "<=":
      result[0] = result[1] <= 0;
      break;
    case "=":
      result[0] = result[1] === 0;
      break;
    case ">":
      result[0] = result[1] > 0;
      break;
    case ">=":
      result[0] = result[1] >= 0;
      break;
  }
  result[1] = parseInt(result[1]);
  return result;
};

console.log(
  "test1: ",
  handleInequation(
    [
      [2.3, 3, 5.6, 7, 6],
      [11, 3, 8.6, 25, 1],
      [0.3, 9, 5.3, 66, 7.8],
    ],
    [1, 3, 2, 7, 5],
    [340, 670, 80.6],
    ["<=", "<=", "<="]
  )
);

console.log(
  "test2: ",
  handleInequation(
    [
      [2.36, 3, 6, 7.1, 6],
      [1, 30, 8.6, 2.5, 21],
      [0.3, 69, 5.3, 6.6, 7.8],
    ],
    [1, 13, 2, 17, 5],
    [340, 67, 300.6],
    ["<=", ">=", "<="]
  )
);
