/**
 * todo 检查是否存在满足条件的数字组合
 * @date 2024/1/2 - 10:54:51
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定一个正整数数组，检查数组中是否存在满足规则的数字组合
 *    规则：A = B + 2C
 * 输入描述
 *    第一个参数：数组长度
 *    第二个参数：正整数数组
 * 输出描述
 *    如果存在满足要求的数，在同一行里依次输出规则里A/B/C的取值，用空格隔开
 *    如果不存在，输出0。
 * 示例
 *    输入: (4, [2, 7, 3, 0]) => 输出: "732"
 *    输入: (3, [1, 1, 1]) => 输出: "0"
 * 备注:
 *    1. 数组长度在3-100之间。
 *    2. 数组成员为0-65535，数组成员可以重复，但每个成员只能在结果算式中使用一次。
 *    如：数组成员为[0, 0, 1, 5]，0出现2次是允许的，但结果0 = 0 + 2 * 0是不允许的，因为算式中使用了3个0
 *    3. 用例保证每组数字里最多只有一组符合要求的解。
 */
const handleNumRuleCombination = (count, arr) => {
  /** 校验输入参数 */
  if (!count || !arr) {
    return console.error("参数缺失，请核实！");
  }
  if (!determineInteger(count)) {
    return console.error("count 必须为整数！");
  }
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是数组");
  }
  if (arr.length < 3 || arr.length > 100) {
    return console.error("arr 长度范围[3, 100]");
  }
  if (count !== arr.length) {
    return console.error("count 必须和 arr 长度保持一致！");
  }
  const flag = arr.every(
    (item) => determineInteger(item) && item >= 0 && item <= 65535
  );
  if (!flag) {
    return console.error("数组中每一项范围【0,65535】整数");
  }

  /** 核心代码区域 */
  const map = new Map();
  for (let i = 0; i < arr.length; i++) {
    if (map.has(arr[i])) {
      map.set(i, map.get(i) + 1);
    } else {
      map.set(i, 1);
    }
  }
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr.length; j++) {
      let res = arr[i] + 2 * arr[j];
      let index = arr.indexOf(res);
      if (index !== -1) {
        result.push([arr[index], arr[i], arr[j]]);
      }
    }
  }
  result = result.filter((item) => {
    if (item[2] === 0) {
      if (item[1] === 0) {
        if (map.get(item[1]) >= 3) {
          return true;
        } else {
          return false;
        }
      } else {
        if (map.get(arr[1] >= 2)) {
          return true;
        } else {
          return false;
        }
      }
    } else {
      if (item[1] === item[2]) {
        if (map.get(item[2]) >= 2) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    }
  });
  return result.length === 0 ? 0 : result;
};

console.log("test1: ", handleNumRuleCombination(4, [2, 7, 3, 0]));
console.log("test2: ", handleNumRuleCombination(3, [1, 1, 1]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
