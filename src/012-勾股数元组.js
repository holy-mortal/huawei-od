/**
 * todo 勾股数元组
 * @date 2023/12/26 - 15:20:40
 * @author Holy-Mortal
 */

/**
 * 需求
 *    如果3个正整数(a,b,c)满足 a * a + b * b = c * c 的关系，则称(a,b,c)为勾股数（著名的勾三股四弦五）
 *    为了探索勾股数的规律，我们定义
 *        如果勾股数(a,b,c)之间两两互质（即a与b，a与c，b与c之间均互质，没有公约数），则其为勾股数元祖
 *        （例如(3,4,5)是勾股数元祖，(6,8,10)则不是勾股数元祖）
 *    请求出给定范围[N,M]内，所有的勾股数元祖
 * 输入描述
 *    起始范围 N : [1,10000]
 *    起始范围 M : [1,10000]
 * 输出描述
 *    1.  a,b,c请保证a < b < c,输出格式：a b c；
 *    2.  多组勾股数元祖请按照a升序，b升序，最后c升序的方式排序输出；
 *    3.  给定范围中如果找不到勾股数元祖时，输出”NA”。
 * 示例
 *    输入: (1, 20) => 输出: [[3, 4, 5], [5, 12, 13], [8, 15, 17]]
 *    输入: (5, 10) => 输出: "NA", [[6, 8, 10]], [5, 10]范围内勾股数有：(6 8 10)；其中，没有满足(a,b,c)之间两两互质的勾股数元祖； 给定范围中找不到勾股数元祖，输出”NA”
 */
const handleHookByte = (start, end) => {
  /** 校验输入参数 */
  if (start === undefined || end === undefined) {
    return console.error("参数缺失，请核实！");
  }
  if (!determineInteger(start) || !determineInteger(end)) {
    return console.error("start、end 参数必须为整数");
  }
  if (start < 1 || start > 10000 || end < 1 || end > 10000) {
    return console.error("start、end的取值范围为[1,10000]");
  }
  if (start >= end) {
    return console.error("start参数必须小于end参数");
  }

  /** 核心代码区域 */
  let result = [];
  for (let a = start; a < end; a++) {
    for (let b = a + 1; b < end; b++) {
      for (let c = b + 1; c < end; c++) {
        if (a * a + b * b === c * c && isManyCoprime([a, b, c])) {
          result.push([a, b, c]);
        }
      }
    }
  }
  return result.length > 0 ? result : "NA";
};

console.log("test1: ", handleHookByte(1, 20));
console.log("test2: ", handleHookByte(5, 10));

/**
 * 两个数互质
 * @param {Number} a
 * @param {Number} b
 * @returns {boolean}
 */
function isTwiceCoprime(a, b) {
  if (a === b) {
    return false;
  }
  for (let i = 2; i <= Math.min(a, b); i++) {
    if (a % i === 0 && b % i === 0) {
      return false;
    }
  }
  return true;
}

function isManyCoprime(nums) {
  if (!Array.isArray(nums)) {
    return false;
  }
  for (let i = 0; i < nums.length; i++) {
    for (let j = i + 1; j < nums.length; j++) {
      if (!isTwiceCoprime(nums[i], nums[j])) {
        return false;
      }
    }
  }
  return true;
}

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
