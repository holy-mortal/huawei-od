/**
 * todo 执行时长
 * @date 2023/12/18 - 08:43:57
 * @author Holy-Mortal
 */

/**
 * 需求
 *    为了充分发挥GPU算力，需要尽可能多的将任务交给GPU执行
 *    现在有一个任务数组，数组元素表示在这1秒内新增的任务个数且每秒都有新增任务
 *    假设GPU最多一次执行n个任务，一次执行耗时1秒，在保证GPU不空闲情况下，最少需要多长时间执行完成
 * 输入描述
 *    第一个参数为GPU一次最多执行的任务个数，取值范围[1, 10000]
 *    第二个参数为任务数组长度，取值范围[1, 10000]
 *    第三个参数为任务数组，数字范围[1, 10000]
 * 输出描述
 *    执行完所有任务最少需要多少秒
 * 示例
 *    输入: (3, 5, [1, 2, 3, 4, 5]) => 输出: 6, 一次最多执行3个任务，最少耗时6s
 *    输入: (4, 5, [5, 4, 1, 1, 1]) => 输出: 5, 一次最多执行4个任务，最少耗时5s
 */
const handleExecutionTime = (maxTaskCount, taskLength, taskArr) => {
  /** 校验输入参数 */
  if (!maxTaskCount || !taskLength || !taskArr) {
    return console.error("参数缺失，请核实！");
  }
  if (!determineInteger(maxTaskCount)) {
    return console.error("maxTaskCount 必须是一个整数");
  }
  if (maxTaskCount < 1 || maxTaskCount > 10000) {
    return console.error("maxTaskCount 必须在[1, 10000]之间");
  }
  if (!determineInteger(taskLength)) {
    return console.error("taskLength 必须是一个整数");
  }
  if (taskLength < 1 || taskLength > 10000) {
    return console.error("taskLength 必须在[1, 10000]之间");
  }
  if (!Array.isArray(taskArr)) {
    return console.error("taskArr 必须是一个javascript数组!");
  }
  if (taskLength !== taskArr.length) {
    return console.error("taskLength 必须等于 taskArr 数组的长度");
  }
  const flag = taskArr.every((task) => {
    return determineInteger(task) && task >= 1 && task <= 10000;
  });
  if (!flag) {
    return console.error("taskArr 数组元素必须是数字且在[1, 10000]之间");
  }

  /** 核心代码区域 */
  let oldArr = JSON.parse(JSON.stringify(taskArr));
  let len = taskLength;
  for (let i = 0; i < len; i++) {
    if (taskArr[i] > maxTaskCount) {
      let diffValue = taskArr[i] - maxTaskCount;
      if (i === len - 1) {
        taskArr.push(diffValue);
      } else {
        taskArr[i + 1] += diffValue;
      }
      taskArr[i] = maxTaskCount;
    }
  }
  /** 前后数组对比 */
  console.log({
    oldArr,
    newArr: taskArr,
  });
  // 结果数组长度，即为需要的最短时间
  return taskArr.length;
};

console.log("test1: ", handleExecutionTime(3, 5, [1, 2, 3, 4, 5]));
console.log("test2: ", handleExecutionTime(4, 5, [5, 4, 1, 1, 1]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
