/**
 * todo 两数之和绝对值最小
 * @date 2024/1/3 - 09:35:58
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定一个从小到大的有序整数序列（存在正整数和负整数）数组 nums ，
 *    请你在该数组中找出两个数，其和的绝对值(|nums[x]+nums[y]|)为最小值，并返回这个绝对值。
 *    每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍
 * 输入描述
 *    一个通过空格分割的有序整数序列字符串，最多1000个整数，且整数数值范围是 -65535~65535
 * 输出描述
 *    两数之和绝对值最小值
 * 示例
 *    输入: ([-3, -1, 5, 7, 11, 15]) => 输出: 2, 因为 |nums[0] + nums[2]| = |-3 + 5| = 2 最小，返回 2
 */
const handleSumMinAbs = (arr) => {
  /** 校验输入参数 */
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  const flag = arr.every(
    (item) => determineInteger(item) && item >= -65535 && item <= 65535
  );
  if (!flag) {
    return console.error("arr 中每一项为整数，且取值范围 [-65535, 65535]");
  }

  /** 核心代码区域 */
  arr.sort((a, b) => a - b);
  let min = Number.MAX_SAFE_INTEGER;
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      let temp = Math.abs(arr[i] + arr[j]);
      if (temp < min) {
        min = temp;
      }
    }
  }
  return min;
};

console.log("test: ", handleSumMinAbs([-3, -1, 5, 7, 11, 15]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
