/**
 * todo 非严格递增连续数字序列
 * @date 2023/12/18 - 14:08:41
 * @author Holy-Mortal
 */

/**
 * 需求
 *    输入一个字符串仅包含大小写字母和数字，求字符串中包含的最长的非严格递增连续数字序列的长度
 *    （比如12234属于非严格递增连续数字序列）
 * 输入描述
 *    输入一个字符串仅包含大小写字母和数字，输入的字符串最大不超过255个字符
 * 输出描述
 *    最长的非严格递增连续数字序列的长度
 * 示例
 *    输入: (abc2234019A334bc) => 输出: 4, 2234为最长的非严格递增连续数字序列，所以长度为4
 */
const handleNoStrictIncreasingNumber = (str) => {
  /** 校验输入参数 */
  if (!str || typeof str !== "string") {
    return console.error("参数缺失或者参数非字符串类型！");
  }
  if (str.length > 255) {
    return console.error("输入的字符串最大不超过255个字符！");
  }
  const rules = /^[0-9a-zA-Z]+$/;
  if (!rules.test(str)) {
    return console.error("字符串必须仅包含大小写字母和数字！");
  }

  /** 核心代码区域 */
  /** 获取所有连续数字字符串的数组 */
  const arr = str.match(/\d+/g);
  const result = [];
  for (let i = 0; i < arr.length; i++) {
    const numStr = arr[i];
    let maxLength = 1;
    let endIndex = 0;
    /** 数组的每一项表示以该项为结尾的最大长度 */
    const dp = new Array(numStr.length).fill(1);
    for (let j = 1; j < numStr.length; j++) {
      if (Number(numStr[j]) >= Number(numStr[j - 1])) {
        dp[j] = dp[j - 1] + 1;
      }
      if (dp[j] > maxLength) {
        maxLength = dp[j];
        endIndex = j;
      }
    }
    let startIndex = endIndex - maxLength + 1;
    let maxLengthStr = numStr.slice(startIndex, endIndex + 1);
    result.push({
      maxLength,
      maxLengthStr,
    });
  }
  // 按最长字串从大到小排列
  result.sort((a, b) => b.maxLength - a.maxLength);
  return result[0];
};

console.log("test: ", handleNoStrictIncreasingNumber("abc2234019A334bc"));
