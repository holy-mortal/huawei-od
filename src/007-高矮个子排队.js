/**
 * todo 高矮个子排队
 * @date 2023/12/19 - 16:03:22
 * @author Holy-Mortal
 */

/**
 * 需求
 *    现在有一队小朋友，他们高矮不同，我们以正整数数组表示这一队小朋友的身高，如数组{5,3,1,2,3}
 *    我们现在希望小朋友排队，以“高”“矮”“高”“矮”顺序排列
 *        每一个“高”位置的小朋友要比相邻的位置高或者相等
 *        每一个“矮”位置的小朋友要比相邻的位置矮或者相等
 *        要求小朋友们移动的距离和最小，第一个从“高”位开始排，输出最小移动距离即可
 *            例如，在示范小队{5,3,1,2,3}中，{5, 1, 3, 2, 3}是排序结果。
 *        {5, 2, 3, 1, 3} 虽然也满足“高”“矮”“高”“矮”顺序排列，但小朋友们的移动距离大，所以不是最优结果
 *    移动距离的定义如下所示：
 *        第二位小朋友移到第三位小朋友后面，移动距离为1，若移动到第四位小朋友后面，移动距离为2
 * 输入描述
 *    第一个参数为正整数身高数组，数组长度 < 100
 * 输出描述
 *    排序后的数组
 * 示例
 *    输入: ([4, 1, 3, 5, 2]) => 输出: [4, 1, 5, 2, 3]
 *    输入: ([1, 1, 1, 1, 1]) => 输出: [1, 1, 1, 1, 1]
 *    输入: (xxx) => 输出: []
 */
const handleHeightQueue = (arr) => {
  /** 校验输入参数 */
  if (!arr || !Array.isArray(arr)) {
    return [];
  }
  if (arr.length >= 100) {
    return console.error("小朋友数量必须 < 100");
  }
  const flag = arr.every((item) => determineInteger(item));
  if (!flag) {
    return console.error("arr 的每个元素都必须是正整数");
  }

  /** 核心代码区域 */
  for (let i = 0; i < arr.length - 1; i++) {
    // 奇数项，如果当前项 < 下一项, 换位置
    if (i % 2 === 0 && arr[i] < arr[i + 1]) {
      let temp = arr[i];
      arr[i] = arr[i + 1];
      arr[i + 1] = temp;
    }
    // 偶数项，如果当前项 > 下一项, 换位置
    if (i % 2 === 1 && arr[i] > arr[i + 1]) {
      let temp = arr[i];
      arr[i] = arr[i + 1];
      arr[i + 1] = temp;
    }
  }
  return arr;
};

console.log("test1: ", handleHeightQueue([4, 1, 3, 5, 2]));
console.log("test2: ", handleHeightQueue([1, 1, 1, 1, 1]));
console.log("test3: ", handleHeightQueue("xxx"));
console.log("test4: ", handleHeightQueue([4, 3, 5, 7, 8]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
