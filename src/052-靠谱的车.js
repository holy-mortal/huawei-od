/**
 * todo 靠谱的车
 * @date 2024/1/5 - 13:14:26
 * @author Holy-Mortal
 */

/**
 * 需求
 *    程序员小明打了一辆出租车去上班。出于职业敏感，他注意到这辆出租车的计费表有点问题，总是偏大。
 *    出租车司机解释说他不喜欢数字4，所以改装了计费表，任何数字位置遇到数字4就直接跳过，其余功能都正常
 *    比如
 *        1. 23 再多一块钱变为 25；
 *        2. 39 再多一块钱变为 50；
 *        3. 399再多一块钱变为 500
 *    小明识破了司机的伎俩，准备利用自己的学识打败司机的阴谋。
 *    给出计费表的表面读数，返回实际产生的费用
 * 输入描述
 *    参数：里程表读数N，取值范围 [1, 888888888]
 * 输出描述
 *    实际产生的费用
 * 示例
 *    输入: (5) => 4, 5表示计费表的表面读数。4表示实际产生的费用其实只有4块钱
 *    输入: (17) => 15, 17表示计费表的表面读数。15表示实际产生的费用其实只有15块钱。
 *    输入: (100) => 81, 100表示计费表的表面读数。81表示实际产生的费用其实只有81块钱。
 */
const handleCarFee = (fee) => {
  /** 校验输入参数 */
  if (typeof fee !== "number") {
    return console.error("fee 必须是一个数字");
  }
  if (!determineInteger(fee)) {
    return console.error("fee 必须是一个整数！");
  }
  if (fee < 1 || fee > 888888888) {
    return console.error("fee 取值范围[1, 888888888]");
  }

  /** 核心代码区域 */
  let count = 0;
  for (let i = 1; i <= fee; i++) {
    if (String(i).includes("4")) {
      count++;
    }
  }
  return fee - count;
};
console.log("test1: ", handleCarFee(5));
console.log("test2: ", handleCarFee(17));
console.log("test3: ", handleCarFee(100));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
