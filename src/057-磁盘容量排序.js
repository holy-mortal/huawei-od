/**
 * todo 磁盘容量排序
 * @date 2024/1/5 - 16:13:37
 * @author Holy-Mortal
 */

/**
 * 需求
 *    磁盘的容量单位常用的有M，G，T这三个等级，它们之间的换算关系为1T = 1024G，1G = 1024M，
 *    现在给定n块磁盘的容量，请对它们按从小到大的顺序进行稳定排序，
 *    例如给定5块盘的容量，1T，20M，3G，10G6T，3M12G9M排序后的结果为20M，3G，3M12G9M，1T，10G6T。
 *    注意单位可以重复出现，上述3M12G9M表示的容量即为3M+12G+9M，和12M12G相等。
 * 输入描述
 *    磁盘数量，取值范围 [2, 100]
 *    磁盘容量数组，每一项用字符串表示，一个字符串(长度大于等于2，小于等于30)，表示磁盘的容量，由一个或多个格式为mv的子串组成，其中m表示容量大小，v表示容量单位，例如20M，1T，30G，10G6T，3M12G9M。
 *    磁盘容量m的范围为1到1024的正整数，容量单位v的范围只包含题目中提到的M，G，T三种
 * 输出描述
 *    n块磁盘容量排序后的结果
 * 示例
 *    输入: (3, ["1G", "2G", "1024M"]) => 输出: ["1G", "1024M", "2G"], 1G和1024M容量相等，稳定排序要求保留它们原来的相对位置，故1G在1024M之前
 *    输入: (3, ["2G4M", "3M2G", "1T"]) => 输出: ["3M2G", "2G4M", "1T"], 1T的容量大于2G4M，2G4M的容量大于3M2G
 */
const handleContantSort = (count, arr) => {
  /** 校验输入参数 */
  if (!determineInteger(count)) {
    return console.error("count 必须是一个整数！");
  }
  if (count < 2 || count > 100) {
    return console.error("count 取值范围 [2, 100]");
  }
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组");
  }
  const rule = /^(([1-9][0-9]{0,2}|10[0-2][0-4])[GMT])+$/;
  const flag = arr.every(
    (item) =>
      typeof item === "string" &&
      item.length >= 2 &&
      item.length <= 30 &&
      rule.test(item)
  );
  if (!flag) {
    return console.error("arr 中每一项为字符串，且必须符合要求！");
  }

  /** 核心代码区域 */
  const example = {
    T: 1024 * 1024,
    G: 1024,
    M: 1,
  };
  function sum(str) {
    return str
      .match(/([1-9][0-9]{0,2}|10[0-2][0-4])[GMT]/g)
      .map(
        (item) =>
          Number(item.substring(0, item.length - 1)) *
          example[item[item.length - 1]]
      )
      .reduce((prev, curr) => {
        return (prev += curr);
      });
  }
  return arr.sort((a, b) => sum(a) - sum(b));
};

console.log("test1: ", handleContantSort(3, ["1G", "2G", "1024M"]));
console.log("test2: ", handleContantSort(3, ["2G4M", "3M2G", "1T"]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
