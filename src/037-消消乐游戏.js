/**
 * todo 消消乐游戏
 * @date 2024/1/2 - 09:45:54
 * @author Holy-Mortal
 */

/**
 * 需求
 *    游戏规则：输入一个只包含英文字母的字符串，字符串中的两个字母如果相邻且相同，就可以消除
 *    在字符串上反复执行消除的动作，直到无法继续消除为止，此时游戏结束。
 *    输出最终得到的字符串长度
 * 输入描述
 *    输入原始字符串 str ，只能包含大小写英文字母，字母的大小写敏感， str 长度不超过100。
 * 输出描述
 *    输出游戏结束后，最终得到的字符串长度
 * 示例
 *    输入: ("gg") => 输出: 0
 *    输入: ("mMbccbc") => 输出: 3
 * 备注:
 *    输入中包含 非大小写英文字母 时，均为异常输入，直接返回 0
 */
const handleMatch3Game = (str) => {
  /** 校验输入参数 */
  if (!str && str !== "") {
    return console.error("参数异常，请核实！");
  }
  if (str.length > 100) {
    return console.error("str 长度不超过 100");
  }
  const rule = /^[a-zA-Z]+$/;
  if (!rule.test(str)) {
    console.error("str 只包含大小写字母！");
    return 0;
  }

  /** 核心代码区域 */
  let arr = str.split("");
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] === arr[i - 1]) {
      arr.splice(i - 1, 2);
      i = i - 2;
    }
  }
  return [arr.join(""), arr.length];
};

console.log("test1: ", handleMatch3Game("gg"));
console.log("test2: ", handleMatch3Game("mMbccbc"));
