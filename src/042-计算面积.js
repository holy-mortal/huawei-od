/**
 * todo 计算面积
 * @date 2024/1/2 - 14:13:23
 * @author Holy-Mortal
 */

/**
 * 需求
 *    绘图机器的绘图笔初始位置在原点（0, 0），机器启动后其绘图笔按下面规则绘制直线：
 *    1）尝试沿着横向坐标轴正向绘制直线，直到给定的终点值E。
 *    2）期间可通过指令在纵坐标轴方向进行偏移，并同时绘制直线，偏移后按规则1 绘制直线；
 *        指令的格式为X offsetY，表示在横坐标X 沿纵坐标方向偏移，
 *        offsetY为正数表示正向偏移，为负数表示负向偏移。
 *    给定了横坐标终点值E、以及若干条绘制指令，请计算绘制的直线和横坐标轴、以及 X=E 的直线组成图形的面积
 * 输入描述
 *    首行为两个整数 N E，表示有N条指令，机器运行的横坐标终点值E。
 *    接下来N行，每行两个整数表示一条绘制指令X offsetY，
 *    用例保证横坐标X以递增排序方式出现，且不会出现相同横坐标X。
 *    取值范围：0 < N <= 10000, 0 <= X <= E <=20000, -10000 <= offsetY <= 10000。
 * 输出描述
 *    一个整数，表示计算得到的面积，用例保证，结果范围在0~4294967295内
 * 示例
 *    输入: (4, 10, [[1, 1], [2, 1], [3, 1], [4, -2]]) => 输出: 12
 *    输入: (2, 4, [[0, 1], [2, -2]]) => 输出: 4
 */
const handleCalcArea = (N, E, matrix) => {
  /** 校验输入参数 */
  if (!determineInteger(N) || !determineInteger(E)) {
    return console.error("N、E必须是整数");
  }
  if (N <= 0 || N > 10000) {
    return console.error("N 必须取值 (0, 10000]");
  }
  if (E < 0 || E > 20000) {
    return console.error("E 必须取值 [0, 20000]");
  }
  if (!Array.isArray(matrix)) {
    return console.error("matrix 必须是一个数组！");
  }
  if (N !== matrix.length) {
    return console.error("N 和 matrix 长度必须保持一致");
  }
  const flag = matrix.every((item) => {
    return (
      item[0] >= 0 &&
      item[0] <= 20000 &&
      item[1] >= -10000 &&
      item[1] <= 10000 &&
      item.every((it) => determineInteger(it))
    );
  });
  if (!flag) {
    return console.error("matrix 必须满足条件！");
  }

  /** 核心代码区域 */
  for (let i = 1; i < matrix.length; i++) {
    matrix[i][1] = matrix[i - 1][1] + matrix[i][1];
  }
  for (let i = 0; i < matrix.length; i++) {
    if (i === matrix.length - 1) {
      matrix[i] = [matrix[i], [E, matrix[i][1]]];
    } else {
      matrix[i] = [matrix[i], [matrix[i + 1][0], matrix[i][1]]];
    }
  }
  let result = 0;
  for (let i = 0; i < matrix.length; i++) {
    let arr = matrix[i];
    result += Math.abs(arr[1][1] - 0) * (arr[1][0] - arr[0][0]);
  }
  console.log(matrix);
  return result;
};

console.log(
  "test1: ",
  handleCalcArea(4, 10, [
    [1, 1],
    [2, 1],
    [3, 1],
    [4, -2],
  ])
);
console.log(
  "test1: ",
  handleCalcArea(2, 4, [
    [0, 1],
    [2, -2],
  ])
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
