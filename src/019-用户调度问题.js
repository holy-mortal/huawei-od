/**
 * todo 用户调度问题
 * @date 2023/12/28 - 12:41:18
 * @author Holy-Mortal
 */

/**
 * 需求
 *    在通信系统中，一个常见的问题是对用户进行不同策略的调度，会得到不同的系统消耗和性能
 *    假设当前有n个待串行调度用户，每个用户可以使用A/B/C三种不同的调度策略
 *    不同的策略会消耗不同的系统资源。请你根据如下规则进行用户调度，并返回总的消耗资源数
 * 规则
 *    1.  相邻的用户不能使用相同的调度策略
 *        例如，第1个用户使用了A策略，则第2个用户只能使用B或者C策略
 *    2.  对单个用户而言，不同的调度策略对系统资源的消耗可以归一化后抽象为数值
 *        例如，某用户分别使用A/B/C策略的系统消耗分别为15/8/17
 *    3.  每个用户依次选择当前所能选择的对系统资源消耗最少的策略（局部最优）
 *        如果有多个满足要求的策略，选最后一个
 * 输入描述
 *    第一个参数表示用户个数
 *    第二个参数表示每个用户在三种策略下的值
 * 输出描述
 *    最优策略组合下的总的系统资源消耗数
 * 示例
 *    输入: (3, [[15, 8, 17], [12, 20, 9], [11, 7, 5]]) => 输出: 24
 */

const handleUserScheduling = (count, matirx) => {
  /** 校验输入参数 */
  if (!count || !matirx) {
    return console.error("参数缺失，请核实！");
  }
  if (!determineInteger(count)) {
    return console.error("count 必须是一个整数");
  }
  if (!Array.isArray(matirx)) {
    return console.error("matirx 必须是一个数组！");
  }
  if (count !== matirx.length) {
    return console.error("count 必须和 matirx 长度保持一致");
  }
  const flag = matirx.every((item) => {
    return (
      Array.isArray(item) &&
      item.length === 3 &&
      item.every((it) => {
        return typeof it === "number";
      })
    );
  });
  if (!flag) {
    return console.error("matirx 为一个矩阵数组，每一个元素均为数字！");
  }

  /** 核心代码区域 */
  function permutateCombinate(matirx, m) {
    const result = [];
    function recutsion(combinator, remain, example) {
      if (combinator.length === m) {
        result.push(combinator);
        return;
      }
      for (let i = 0; i < remain.length; i++) {
        const temp = example.filter((item) => item !== remain[i]);
        recutsion([...combinator, remain[i]], temp, example);
      }
    }
    const arr = new Array(matirx.length)
      .fill(0)
      .map((item, index) => item + index);
    recutsion([], arr, arr);
    return result;
  }
  const temp = permutateCombinate(matirx, count);
  const map = new Map();
  for (let i = 0; i < temp.length; i++) {
    const item = temp[i];
    let key = matirx[0][item[0]] + matirx[1][item[1]] + matirx[2][item[2]];
    if (map.has(key)) {
      let tempArr = map.get(key);
      tempArr.push([
        matirx[0][item[0]],
        matirx[1][item[1]],
        matirx[2][item[2]],
      ]);
      map.set(key, tempArr);
    } else {
      map.set(key, [
        [matirx[0][item[0]], matirx[1][item[1]], matirx[2][item[2]]],
      ]);
    }
  }
  const result = Array.from(map).sort((a, b) => a[0] - b[0]);
  return result[0];
};

console.log(
  "test: ",
  handleUserScheduling(3, [
    [15, 8, 17],
    [12, 20, 9],
    [11, 7, 5],
  ])
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
