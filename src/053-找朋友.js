/**
 * todo 找朋友
 * @date 2024/1/5 - 13:34:17
 * @author Holy-Mortal
 */

/**
 * 需求
 *    在学校中，N个小朋友站成一队， 第i个小朋友的身高为 height[i]，
 *    第i个小朋友可以看到的第一个比自己身高更高的小朋友j，那么j是i的好朋友(要求j > i)。
 *    请重新生成一个列表，对应位置的输出是每个小朋友的好朋友位置，
 *    如果没有看到好朋友，请在该位置用0代替。
 *    小朋友人数范围是 [0, 40000]。
 * 输入描述
 *    第一个参数：N 表示小朋友数量
 *    第二个参数：N 个小朋友的身高数组，均为整数
 * 输出描述
 *    输出N个小朋友的好朋友的位置
 * 示例
 *    输入: (2, [100, 95]) => 输出: [0, 0], 第一个小朋友身高100，站在队尾位置，向队首看，没有比他身高高的小朋友，所以输出第一个值为0。第二个小朋友站在队首，前面也没有比他身高高的小朋友，所以输出第二个值为0。
 *    输入: (8, [123, 124, 125, 121, 119, 122, 126, 123]) => 输出: [1, 2, 6, 5, 5, 6, 0, 0]
 */
const handleFindFriend = (count, arr) => {
  /** 校验输入参数 */
  if (!determineInteger(count)) {
    return console.error("count 必须是一个整数！");
  }
  if (count < 0 || count > 40000) {
    return console.error("count 取值范围 [0, 40000]！");
  }
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  if (count !== arr.length) {
    return console.error("count 必须和 arr 长度保持一致！");
  }
  const flag = arr.every((item) => determineInteger(item) && item > 0);
  if (!flag) {
    return console.error("arr 中每一项必须是整数");
  }

  /** 核心代码区域 */
  let result = new Array(count).fill(0);
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] > arr[i]) {
        result[i] = j;
        break;
      }
    }
  }
  return result;
};
console.log("test1: ", handleFindFriend(2, [100, 95]));
console.log(
  "test1: ",
  handleFindFriend(8, [123, 124, 125, 121, 119, 122, 126, 123])
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
