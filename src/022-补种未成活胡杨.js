/**
 * todo 补种未成活胡杨
 * @date 2023/12/28 - 17:49:03
 * @author Holy-Mortal
 */

/**
 * 需求
 *    近些年来，我国防沙治沙取得显著成果。某沙漠新种植N棵胡杨（编号1-N），排成一排。
 *    一个月后，有M棵胡杨未能成活
 *    现可补种胡杨K棵，请问如何补种（只能补种，不能新种），可以得到最多的连续胡杨树？
 * 输入描述
 *    总种植数量 N
 *    未成活胡杨数量 M
 *    M 个空格分隔的数，按编号从小到大排列
 *    K 最多可以补种的数量
 *    其中
 *        1 <= N <= 100000
 *        1 <= M <= N
 *        0 <= K <= M
 * 输出描述
 *    最多的连续胡杨树
 * 示例
 *    输入: (5, 2, [2, 4], 1) => 输出: 3, 补种到2或4结果一样，最多的连续胡杨棵树都是3
 *    输入: (10, 3, [2, 4, 7], 1) => 输出: 6, 补种第7棵树，最多的连续胡杨棵树为6(5,6,7,8,9,10)
 */
const handleMaxTreeSort = (N, M, P, K) => {
  /** 校验输入参数 */
  if (!determineInteger(N) || !determineInteger(M) || !determineInteger(K)) {
    return console.error("N、M、K 必须是整数！");
  }
  if (!Array.isArray(P)) {
    return console.error("P 必须是一个数组！");
  }
  if (N < 1 || N > 100000) {
    return console.error("N 取值范围 [1, 100000]");
  }
  if (M < 1 || M > N) {
    return console.error("M 取值范围 [1, N]");
  }
  if (K < 0 || K > M) {
    return console.error("K 取值范围 [0, M]");
  }
  if (M !== P.length) {
    return console.error("P 数组长度必须等于 M");
  }
  const flag = P.every((item) => determineInteger(item) && item <= N);
  if (!flag) {
    return console.error("P 数组每一项均为数字");
  }
  if (K >= P.length) {
    return {
      location: P,
      length: N,
      tree: new Array(N).fill(1).map((item, index) => item + index),
    };
  }

  /** 核心代码区域 */

  function combination(arr, m) {
    let result = [];
    arr.sort((a, b) => a - b);
    function recursion(res, remain) {
      if (res.length === m) {
        result.push(res);
      }
      for (let i = 0; i < remain.length; i++) {
        recursion(
          [...res, remain[i]],
          [...remain.slice(0, i), ...remain.slice(i + 1)]
        );
      }
    }
    recursion([], arr);
    return Array.from(
      new Set(result.map((item) => item.sort((a, b) => a - b).join("-")))
    ).map((item) => item.split("-").map((it) => Number(it)));
  }
  const combinator = combination(
    P.map((item) => item - 1),
    K
  );

  const temp = [];
  for (let i = 0; i < combinator.length; i++) {
    let item = combinator[i];
    let arr = new Array(N).fill(1).map((item, index) => {
      return P.includes(index + 1) ? 0 : 1;
    });
    for (let j = 0; j < item.length; j++) {
      arr[item[j]] = 1;
    }
    temp.push(translate(arr, item));
  }
  const map = new Map();
  for (let i = 0; i < temp.length; i++) {
    const keys = Array.from(temp[i].keys());
    for (let j = 0; j < keys.length; j++) {
      if (map.has(keys[j])) {
        let tempArr = map.get(keys[j]);
        tempArr.push(temp[i].get(keys[j]));
        map.set(keys[j], tempArr);
      } else {
        map.set(keys[j], [temp[i].get(keys[j])]);
      }
    }
  }
  const result = Array.from(map).sort((a, b) => b[0] - a[0])[0];
  return result[1].map((item) => {
    return {
      location: item.label.map((item) => item + 1),
      length: result[0],
      tree: item.value.map((item) => {
        return new Array(item[1] - item[0] + 1)
          .fill(item[0])
          .map((it, i) => it + i + 1)
          .join("-");
      }),
    };
  });
};

console.log("test1: ", handleMaxTreeSort(5, 2, [2, 4], 1));
console.log("test2: ", handleMaxTreeSort(10, 3, [2, 4, 7], 1));

function translate(arr, item) {
  let len = arr.length - 1;
  let temp = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === 0) {
      temp.push(i);
    }
  }

  const map = new Map();
  for (let i = 1; i < temp.length; i++) {
    const key = temp[i] - temp[i - 1] - 1;
    if (map.has(key)) {
      let tempArr = map.get(key);
      tempArr.value.push([temp[i - 1] + 1, temp[i] - 1]);
      map.set(key, tempArr);
    } else {
      map.set(key, {
        label: item,
        value: [[temp[i - 1] + 1, temp[i] - 1]],
      });
    }
  }
  if (temp[0] !== 0) {
    const key = temp[0];
    if (map.has(key)) {
      let tempArr = map.get(key);
      tempArr.value.unshift([0, temp[0] - 1]);
      map.set(key, tempArr);
    } else {
      map.set(key, {
        label: item,
        value: [[0, temp[0] - 1]],
      });
    }
  }
  if (temp[temp.length - 1] !== len) {
    const key = len - temp[temp.length - 1];
    if (map.has(key)) {
      let tempArr = map.get(key);
      tempArr.value.push([temp[temp.length - 1] + 1, len]);
      map.set(key, tempArr);
    } else {
      map.set(key, {
        label: item,
        value: [[temp[temp.length - 1] + 1, len]],
      });
    }
  }
  return map;
}

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
