/**
 * todo 最长的指定瑕疵度的元音子串
 * @date 2024/1/3 - 09:57:36
 * @author Holy-Mortal
 */

/**
 * 需求
 *    开头和结尾都是元音字母（aeiouAEIOU）的字符串为 元音字符串 ，其中混杂的非元音字母数量为其 瑕疵度
 *    比如:
 *        · “a” 、 “aa”是元音字符串，其瑕疵度都为0
 *        · “aiur”不是元音字符串（结尾不是元音字符）
 *        · “abira”是元音字符串，其瑕疵度为2
 *    给定一个字符串，请找出指定瑕疵度的最长元音字符子串，并输出其长度，
 *    如果找不到满足条件的元音字符子串，输出0。
 *    子串：字符串中任意个连续的字符组成的子序列称为该字符串的子串
 * 输入描述
 *    第一个参数：瑕疵度 flaw，取值范围 [0, 65535]
 *    第二个参数：包含大小写字母的字符串，长度范围 [0, 65535]
 * 输出描述
 *    输出为一个整数，代表满足条件的元音字符子串的长度
 * 示例
 *    输入: (0, "asdbuiodevauufgh") => 输出: 3, 满足条件的最长元音字符子串有两个，分别为uio和auu，长度为3
 *    输入: (2, "aeueo") => 输出: 0, 没有满足条件的元音字符子串，输出0
 *    输入: (1, "aabeebuu") => 输出: 5, 满足条件的最长元音字符子串有两个，分别为aabee和eebuu，长度为5
 */
const handleFlawReMaxLenString = (flaw, str) => {
  /** 校验输入参数 */
  if (!determineInteger(flaw)) {
    return console.error("flaw 必须是一个整数！");
  }
  if (flaw < 0 || flaw > 65535) {
    return console.error("flaw 取值范围 [0, 65535]");
  }
  if (typeof str !== "string") {
    return console.error("str 必须是一个字符串！");
  }
  const rule = /^[a-zA-Z]+$/;
  if (!rule.test(str)) {
    return console.error("str 只包含大小写字母");
  }

  /** 核心代码区域 */
  let caseRule = "aeiouAEIOU".split("");
  let arr = [];
  let newArr = [];
  for (let i = 0; i < str.length; i++) {
    if (caseRule.includes(str[i])) {
      arr.push(i);
      newArr.push(1);
    } else {
      newArr.push(0);
    }
  }
  let tempArr = [];
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      tempArr.push([arr[i], arr[j]]);
    }
  }
  const map = new Map();
  for (let i = 0; i < tempArr.length; i++) {
    const item = tempArr[i];
    const subArr = newArr.slice(item[0], item[1] + 1);
    const key = subArr.filter((item) => item === 0).length;
    if (map.has(key)) {
      const temp = map.get(key);
      temp.push([item[0], item[1]]);
      map.set(key, temp);
    } else {
      map.set(key, [[item[0], item[1]]]);
    }
  }
  const resArr = map.get(flaw);
  if (!resArr) {
    return 0;
  }
  const resMap = new Map();
  for (let i = 0; i < resArr.length; i++) {
    const item = resArr[i];
    const resStr = str.substring(item[0], item[1] + 1);
    const key = item[1] - item[0] + 1;
    if (resMap.has(key)) {
      const temp = resMap.get(key);
      temp.push(resStr);
      resMap.set(key, temp);
    } else {
      resMap.set(key, [resStr]);
    }
  }
  const result = Array.from(resMap).sort((a, b) => b[0] - a[0])[0];
  return result;
};
console.log("test1: ", handleFlawReMaxLenString(0, "asdbuiodevauufgh"));
console.log("test2: ", handleFlawReMaxLenString(2, "aeueo"));
console.log("test3: ", handleFlawReMaxLenString(1, "aabeebuu"));

/**
 * 连续的瑕疵
 * @param {Number} flaw
 * @param {String} str
 */
const handleFlawMaxLenString = (flaw, str) => {
  /** 校验输入参数 */
  if (!determineInteger(flaw)) {
    return console.error("flaw 必须是一个整数！");
  }
  if (flaw < 0 || flaw > 65535) {
    return console.error("flaw 取值范围 [0, 65535]");
  }
  if (typeof str !== "string") {
    return console.error("str 必须是一个字符串！");
  }
  const rule = /^[a-zA-Z]+$/;
  if (!rule.test(str)) {
    return console.error("str 只包含大小写字母");
  }

  /** 核心代码区域 */
  let caseRule = "aeiouAEIOU".split("");
  let arr = [];
  for (let i = 0; i < str.length; i++) {
    if (caseRule.includes(str[i])) {
      arr.push(i);
    }
  }
  const map = new Map();
  for (let i = 0; i < arr.length - 1; i++) {
    const key = arr[i + 1] - arr[i] - 1;
    if (map.has(key)) {
      let temp = map.get(key);
      temp.push([arr[i], arr[i + 1]]);
      map.set(key, temp);
    } else {
      map.set(key, [[arr[i], arr[i + 1]]]);
    }
  }
  let tempArr = map.get(flaw);
  if (!tempArr) {
    return 0;
  }
  for (let i = 1; i < tempArr.length; i++) {
    if (tempArr[i - 1][1] === tempArr[i][0]) {
      tempArr[i - 1][1] = tempArr[i][1];
      tempArr.splice(i, 1);
      i--;
    }
  }
  let result = [0, []];
  result[1] = tempArr.map((item) => str.substring(item[0], item[1] + 1));
  result[0] = result[1][0].length;
  return result;
};

// console.log("test1: ", handleFlawMaxLenString(0, "asdbuiodevauufgh"));
// console.log("test2: ", handleFlawMaxLenString(2, "aeueo"));
// console.log("test3: ", handleFlawMaxLenString(1, "aabeebuu"));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
