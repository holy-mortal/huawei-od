/**
 * todo 停车场车辆统计
 * @date 2023/12/28 - 15:36:31
 * @author Holy-Mortal
 */

/**
 * 需求
 *    特定大小的停车场，数组cars[]表示，其中1表示有车，0表示没车，车辆大小不一
 *    小车占一个车位（长度1），货车占两个车位（长度2），卡车占三个车位（长度3）
 *    统计停车场最少可以停多少辆车，返回具体的数目。
 * 输入描述
 *    整型数组cars[]，其中1表示有车，0表示没车，数组长度小于1000。
 * 输出描述
 *    整型数字，表示最少停车数目
 * 示例
 *    输入: ([1, 0, 1]) => 输出: 2
 *    输入: ([1, 1, 0, 0, 1, 1, 1, 0, 1]) => 输出: 3
 */
const handleCarCount = (arr) => {
  /** 校验输入参数 */
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  if (arr.length >= 1000) {
    return console.error("arr 长度不超过 1000");
  }
  const flag = arr.every(
    (item) => determineInteger(item) && (item === 0 || item === 1)
  );
  if (!flag) {
    return console.error("数组中只包含0/1元素！");
  }

  /** 核心代码区域 */
  let count = 0;
  arr = arr.join("").split("0");
  for (let i = 0; i < arr.length; i++) {
    let len = arr[i].length;
    let j = 3;
    if (len > 0) {
      while (j >= 1) {
        count += parseInt(len / j);
        len %= j;
        j--;
      }
    }
  }
  return count;
};

console.log("test1: ", handleCarCount([1, 0, 1]));
console.log("test2: ", handleCarCount([1, 1, 0, 0, 1, 1, 1, 0, 1]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
