/**
 * todo 分苹果
 * @date 2023/12/19 - 11:01:53
 * @author Holy-Mortal
 */

/**
 * 需求
 *    A、B两个人把苹果分为两堆，A希望按照他的计算规则等分苹果
 *    A计算规则
 *        按照二进制加法计算，并且不计算进位 12+5=9(1100 + 0101=9)
 *    B的计算规则
 *        十进制加法，包括正常进位，B希望在满足A的情况下获取苹果重量最多。
 *        输入苹果的数量和每个苹果重量，输出满足A的情况下B获取的苹果总重量。
 *        如果无法满足A的要求，输出-1。
 * 数据范围
 *    1<= 总苹果数量 <= 20000
 *    1 <= 每个苹果重量 <= 10000
 * 输入描述
 *    第一个参数是苹果数量
 *    第二个参数是苹果质量数组，数组元素为每个苹果的质量
 * 输出描述
 *    输出是B获取的苹果总重量
 * 示例
 *    输入: (3, [3, 5, 6]) => 输出: 11
 *    输入: (8, [7258, 6579, 2602, 6716, 3050, 3564, 5396, 1773]) => 输出: 35165
 */
const handleApplePile = (count, appleArr) => {
  /** 校验输入参数 */
  if (!count || !appleArr) {
    return console.error("参数异常，请核实！");
  }
  if (!determineInteger(count)) {
    return console.error("count 必须是一个整数");
  }
  if (count < 1 || count > 20000) {
    return console.error("必须满足：1<= 总苹果数量 <= 20000");
  }
  if (!Array.isArray(appleArr)) {
    return console.error("appleArr 必须是一个数组类型");
  }
  if (count !== appleArr.length) {
    return console.error("count 必须等于 appleArr 的长度");
  }
  const flag = appleArr.every(
    (item) => determineInteger(item) && item >= 1 && item <= 10000
  );
  if (!flag) {
    return console.error("必须满足：1 <= 每个苹果重量 <= 10000");
  }

  /** 核心代码区域 */

  // 思路：满足 A 条件，表明所有元素求 ^ 异或等于 0
  // 满足 B 条件，则其余全部异或和最小的一个求异或为0
  let sum = 0;
  let minA = Number.MAX_VALUE;
  let binSum = 0;
  for (let i = 0; i < count; i++) {
    // 所有苹果的总质量
    sum += appleArr[i];
    // 累积异或总值
    binSum ^= appleArr[i];
    // A 的最小值
    minA = Math.min(appleArr[i], minA);
  }
  if (binSum === 0) {
    // 获取 B 的最大值
    return sum - minA;
  } else {
    // 不满足 A 条件，则返回 -1
    return -1;
  }
};

console.log("test1: ", handleApplePile(3, [5, 3, 6]));
console.log(
  "test2: ",
  handleApplePile(8, [7258, 6579, 2602, 6716, 3050, 3564, 5396, 1773])
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}

function extendsPass(count, appleArr) {
  // 判断是否能够满足A条件
  const isCompleteA = appleArr.reduce((prev, curr) => prev ^ curr, 0);
  if (isCompleteA) {
    return -1;
  }
  appleArr.sort((a, b) => a - b);
  let max = -1;
  for (let i = 1; i < appleArr.length - 1; i++) {
    let sumBinA = 0;
    let sumBinB = 0;
    let sumA = 0;
    let sumB = 0;
    // 左边
    for (let j = 0; j < i; j++) {
      sumBinA ^= appleArr[j];
      sumA += appleArr[j];
    }
    // 右边
    for (let j = i; j < appleArr.length; j++) {
      sumBinB ^= appleArr[j];
      sumB += appleArr[j];
    }
    if (sumBinA === sumBinB) {
      max = Math.max(Math.max(sumA, sumB), max);
    }
  }
  return max;
}
