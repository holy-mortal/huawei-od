/**
 * todo 求最多可以派出多少支团队
 * @date 2023/12/20 - 09:53:38
 * @author Holy-Mortal
 */

/**
 * 需求
 *    用数组代表每个人的能力，一个比赛活动要求参赛团队的最低能力值为N
 *    每个团队可以由1人或2人组成，且1个人只能参加1个团队，请计算出最多可以派出多少支符合要求的团队？
 * 输入描述
 *    第一个参数表示总人数，范围[1,500000]
 *    第二个参数表示能力数组，长度范围[1,500000]，每个元素的取值范围[1, 500000]
 *    第三个参数表示团队最低能力值，范围[1, 500000]
 * 输出描述
 *    最多可以派出的团队数量
 * 示例
 *    输入: (5, [3,1,5,7,9], 8) => 输出: 3, 3,5组成一队，1,7组成一队，9自己一个队，故输出3
 */
const handleDivideTeams = (count, arr, min) => {
  /** 校验输入参数 */
  if (!count || !arr || !min) {
    return console.error("参数缺失，请核实！");
  }
  if (!determineInteger(count)) {
    return console.error("count参数必须为整数！");
  }
  if (count < 1 || count > 500000) {
    return console.error("count参数必须在[1,500000]范围内！");
  }
  if (!Array.isArray(arr)) {
    return console.error("arr参数必须为数组！");
  }
  if (arr.length < 1 || arr.length > 50000) {
    return console.error("arr参数长度必须在[1,50000]范围内！");
  }
  if (count !== arr.length) {
    return console.error("count参数与arr数组长度不一致！");
  }
  const flag = arr.every(
    (item) =>
      typeof item === "number" &&
      !Number.isNaN(item) &&
      item >= 1 &&
      item <= 500000
  );
  if (!flag) {
    return console.error("arr数组元素必须为数字，且取值范围在[1,50000]！");
  }
  if (min < 1 || min > 500000) {
    return console.error("min参数必须在[1,500000]范围内！");
  }

  /** 核心代码区域 */
  arr.sort((a, b) => a - b);
  const index = arr.findIndex((item) => item >= min);
  const count1 = count - index;
  const newArr = arr.slice(0, index);
  let count2 = 0;
  let leftIndex = 0;
  let rightIndex = newArr.length - 1;
  while (leftIndex < rightIndex) {
    const sum = newArr[leftIndex] + newArr[rightIndex];
    if (sum >= min) {
      leftIndex++;
      rightIndex--;
      count2++;
    } else if (sum < min) {
      leftIndex++;
    }
  }
  return count1 + count2;
};

console.log("test: ", handleDivideTeams(5, [3, 1, 5, 7, 9], 8));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
