/**
 * todo 根据某条件聚类最少交换次数
 * @date 2024/1/5 - 15:37:59
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给出数字K,请输出所有结果小于K的整数组合到一起的最少交换次数
 *    组合一起是指满足条件的数字相邻，不要求相邻后在数组中的位置。
 *    数据范围
 *        -100 <= K <= 100
 *        -100 <=  数组中数值<=  100
 * 输入描述
 *    第一个参数：数组
 *    第二个参数 K
 * 输出描述
 *    最少交换次数
 * 示例
 *    输入: ([1, 3, 1, 4, 0], 2) => 输出: 1
 *    输入: ([0, 0, 0, 1, 0], 2) => 输出: 0
 *    输入: ([2, 3, 2], 1) => 输出: 0
 */
const handleMinTranslateCount = (arr, k) => {
  /** 校验输入参数 */
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  if (!determineInteger(k)) {
    return console.error("k 必须是一个整数");
  }
  if (k < -100 || k > 100) {
    return console.error("k 取值范围 [-100, 100]");
  }
  const flag = arr.every(
    (item) => determineInteger(item) && item >= -100 && item <= 100
  );
  if (!flag) {
    return console.error("arr 数组中每一项取值范围 [-100, 100]");
  }

  /** 核心代码区域 */
  return arr.map((item) => (item < k ? 1 : 0));
};
console.log([1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1]);
console.log("test1: ", handleMinTranslateCount([1, 3, 1, 4, 0], 2));
console.log("test2: ", handleMinTranslateCount([0, 0, 0, 1, 0], 2));
console.log("test3: ", handleMinTranslateCount([2, 3, 2], 1));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
