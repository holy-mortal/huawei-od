/**
 * todo 第 k 个排列
 * @date 2023/12/30 - 19:42:00
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定参数n，从1到n会有n个整数：1,2,3,…,n，这n个数字共有 n! 种排列
 *    按大小顺序升序列出所有排列情况，并一一标记，当 n = 3 时, 所有排列如下
 *    "123" "132" "213" "231" "312" "321"
 *    给定 n 和 k，返回第 k 个排列
 * 输入描述
 *    第一个参数：n，取值范围 [1, 9]
 *    第二个参数：k，取值范围 [1, n!]
 * 输出描述
 *    输出排在第 k 位置的数字
 * 示例
 *    输入: (3, 3) => 输出: 213
 *    输入: (2, 2) => 输出: 21
 */
const handlePermutationCombination = (n, k) => {
  /** 校验输入参数 */
  if (typeof n !== "number" || typeof k !== "number") {
    return console.error("参数均为数字！");
  }
  if (n < 1 || n > 9) {
    return console.error("参数 n 取值范围 [1, 9]!");
  }
  const total = factorial(n);
  if (k < 1 || k > total) {
    return console.error(`参数 k 取值范围 [1, ${total}]!`);
  }

  /** 核心代码区域 */
  const result = [];
  const nums = new Array(n).fill(0).map((it, i) => it + i + 1);
  function permute(nums, index) {
    if (index === nums.length) {
      // 输出当前排列
      result.push(nums.join(""));
    } else {
      for (let i = index; i < nums.length; i++) {
        // 交换当前数字和索引位置的数字
        [nums[i], nums[index]] = [nums[index], nums[i]];
        // 继续递归调用
        permute(nums, index + 1);
        // 恢复原来的数字顺序
        [nums[i], nums[index]] = [nums[index], nums[i]];
      }
    }
  }
  // 开始生成排列
  permute(nums, 0);
  console.log("result: ", result);
  return result[k - 1];
};

console.log("test1: ", handlePermutationCombination(3, 3));
console.log("test2: ", handlePermutationCombination(2, 2));

/**
 * 求 n 的阶乘
 * @param {Number} n
 */
function factorial(n) {
  if (n === 0 || n === 1) {
    // 递归终止条件
    return 1;
  } else {
    return n * factorial(n - 1); // 递归调用
  }
}

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
