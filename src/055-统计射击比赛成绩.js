/**
 * todo 统计射击比赛成绩
 * @date 2024/1/5 - 15:01:17
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定一个射击比赛成绩单，包含多个选手若干次射击的成绩分数，
 *    请对每个选手按其最高3个分数之和进行降序排名，输出降序排名后的选手ID序列。条件如下：
 *    1、一个选手可以有多个射击成绩的分数，且次序不固定。
 *    2、如果一个选手成绩少于3个，则认为选手的所有成绩无效，排名忽略该选手。
 *    3、如果选手的成绩之和相等，则成绩之和相等的选手按照其ID降序排列。
 * 输入描述
 *    第一个参数：射击场次，取值范围 [2, 100]
 *    第二个参数：每场射击参与选手的 id，取值范围 [0, 99]
 *    第三个参数：每场选手对应的分数，取值范围[0, 100]
 * 输出描述
 *    符合题设条件的降序排名后的选手ID序列
 * 示例
 *    输入: (13, [3,3,7,4,4,4,4,7,7,3,5,5,5], [53, 80, 68, 24, 39, 76, 66, 16, 100, 55, 53, 80, 55]) => 输出: [5, 3, 7, 4]
 */
const handleCalcMatchGrade = (count, idArr, gradeArr) => {
  /** 校验输入参数 */
  if (!determineInteger(count)) {
    return console.error("count 必须是整数！");
  }
  if (count < 2 || count > 100) {
    return console.error("count 取值范围 [2, 100]");
  }
  if (!Array.isArray(idArr)) {
    return console.error("idArr 必须是一个数组！");
  }
  if (!Array.isArray(gradeArr)) {
    return console.error("gradeArr 必须是一个数组！");
  }
  if (count !== idArr.length || count !== gradeArr.length) {
    return console.error("count 必须与 idArr 和 gradeArr 长度保持一致！");
  }
  const flag1 = idArr.every(
    (item) => determineInteger(item) && item >= 0 && item <= 99
  );
  const flag2 = gradeArr.every(
    (item) =>
      typeof item === "number" &&
      !Number.isNaN(item) &&
      item >= 0 &&
      item <= 100
  );
  if (!flag1 || !flag2) {
    return console.error("idArr或gradeArr不满足要求");
  }

  /** 核心代码区域 */
  const map = new Map();
  for (let i = 0; i < idArr.length; i++) {
    const key = idArr[i];
    if (map.has(key)) {
      let temp = map.get(key);
      temp.push(gradeArr[i]);
      map.set(key, temp);
    } else {
      map.set(key, [gradeArr[i]]);
    }
  }
  let result = Array.from(map)
    .filter((item) => item[1].length >= 3)
    .map((item) => {
      return {
        id: item[0],
        grade: item[1]
          .sort((a, b) => b - a)
          .slice(0, 3)
          .reduce((prev, curr) => (prev += curr)),
      };
    })
    .sort((a, b) => {
      if (b.grade === a.grade) {
        return b.id - a.id;
      }
      return b.grade - a.grade;
    })
    .map((item) => item.id);
  return result;
};

console.log(
  "test1: ",
  handleCalcMatchGrade(
    13,
    [3, 3, 7, 4, 4, 4, 4, 7, 7, 3, 5, 5, 5],
    [53, 80, 68, 24, 39, 76, 66, 16, 100, 55, 53, 80, 55]
  )
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
