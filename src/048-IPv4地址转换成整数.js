/**
 * todo IPv4地址转换成整数
 * @date 2024/1/4 - 10:14:34
 * @author Holy-Mortal
 */

/**
 * 需求
 *    存在一种虚拟IPv4地址，由4小节组成，每节的范围为0~255，以#号间隔，虚拟IPv4地址可以转换为一个32位的整数
 *    例如：
 *        128#0#255#255，转换为32位整数的结果为2147549183（0x8000FFFF）
 *        1#0#0#0，转换为32位整数的结果为16777216（0x01000000）
 *    现以字符串形式给出一个虚拟IPv4地址，限制第1小节的范围为1~128，
 *    即每一节范围分别为(1~128)#(0~255)#(0~255)#(0~255)，
 *    要求每个IPv4地址只能对应到唯一的整数上。如果是非法IPv4，返回invalid IP
 * 输入描述
 *    虚拟IPv4地址格式字符串
 * 输出描述
 *    按照要求输出整型或者特定字符
 * 示例
 *    输入: ("100#101#1#5") => 输出: 1684340997
 *    输入: ("1#2#3") => 输出: "invalid IP"
 */
const handleIpv4ToInt = (str) => {
  /** 校验输入参数 */
  if (typeof str !== "string" || str === "") {
    return "invalid IP";
  }
  let arr = str.split("#");
  if (arr.length !== 4) {
    return "invalid IP";
  }
  const flag = arr.every((item, index) => {
    let it = Number(item);
    if (index === 0) {
      return it >= 1 && it <= 128;
    }
    return it >= 0 && it <= 255;
  });
  if (!flag) {
    return "invalid IP";
  }

  /** 核心代码区域 */
  const result =
    "0x" +
    arr
      .map((item) => {
        return Number(item).toString(16).padStart(2, "0");
      })
      .join("");
  return Number(result);
};

console.log("test: ", handleIpv4ToInt("128#0#255#255"));
console.log("test1: ", handleIpv4ToInt("100#101#1#5"));
console.log("test2: ", handleIpv4ToInt("1#2#3"));
