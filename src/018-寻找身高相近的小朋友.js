/**
 * todo 寻找身高相近的小朋友
 * @date 2023/12/28 - 12:14:53
 * @author Holy-Mortal
 */
/**
 * 需求
 *    小明今年升学到小学一年级，来到新班级后发现其他小朋友们身高参差不齐
 *    然后就想基于各小朋友和自己的身高差对他们进行排序，请帮他实现排序
 * 输入描述
 *    第一个参数，小明的身高H，0 < H < 200
 *    第二个参数，其他小朋友个数N，0 < N < 50
 *    第三个参数，其他小朋友的身高数组，每个小朋友身高范围(0, 200)
 * 输出描述
 *    和小明身高差绝对值最小的小朋友排在前面，和小明身高差绝对值最大的小朋友排在最后
 *    如果两个小朋友和小明身高差一样，则个子较小的小朋友排在前面
 * 示例
 *    输入: (100, 10, [95, 96, 97, 98, 99, 101, 102, 103, 104, 105]) => 输出: [99, 101, 98, 102, 97, 103, 96, 104, 95, 105]
 */
const handleHeightSort = (height, count, arr) => {
  /** 校验输入参数 */
  if (!height || !count || !arr) {
    return console.error("参数缺失，请核实！");
  }
  if (typeof height !== "number" || typeof count !== "number") {
    return console.error("height/count 必须是数字");
  }
  if (height >= 200 || height <= 0) {
    return console.error("height 范围(0, 200)");
  }
  if (!determineInteger(count)) {
    return console.error("count 必须为整数");
  }
  if (count <= 0 || count >= 50) {
    return console.error("count 范围(1, 50)");
  }
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  if (count !== arr.length) {
    return console.error("count 必须与 arr 长度保持一致");
  }
  const flag = arr.every(
    (item) => typeof item === "number" && item > 0 && item < 200
  );
  if (!flag) {
    return console.error("arr 数组中每一项为数字，且范围(0, 200)");
  }

  /** 核心代码区域 */
  return arr.sort((a, b) => {
    const absA = Math.abs(height - a);
    const absB = Math.abs(height - b);
    if (absA === absB) {
      return a - b;
    } else {
      return absA - absB;
    }
  });
};

console.log(
  "test: ",
  handleHeightSort(100, 10, [95, 96, 97, 98, 99, 101, 102, 103, 104, 105])
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
