/**
 * todo 拼接 URL
 * @date 2023/12/30 - 19:22:10
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定一个URL前缀和URL后缀，通过","分割，需要将其连接为一个完整的URL，
 *    如果前缀结尾和后缀开头都没有“/”，需自动补上“/”连接符，如果前缀结尾和后缀开头都为“/”，需自动去重
 *    约束：不用考虑前后缀URL不合法情况
 * 输入描述
 *    URL前缀（一个长度小于100的字符串),URL后缀（一个长度小于100的字符串）
 * 输出描述
 *    拼接后的URL
 * 示例
 *    输入: ("/acm", "/bb") => 输出: "/acm/bb"
 *    输入: ("/abc/", "/bcd") => 输出: "/abc/bcd"
 *    输入: ("/acd", "bef") => 输出: "/acd/bef"
 *    输入: ("", "") => 输出: "/"
 */
const handleUrlContact = (str1, str2) => {
  /** 校验输入参数 */
  if (typeof str1 !== "string" || typeof str2 !== "string") {
    return console.error("参数均为字符串！");
  }

  /** 核心代码区域 */
  const str = "/" + str1 + "/" + str2;
  const result = str
    .split("/")
    .filter((item) => !!item)
    .join("/");
  return "/" + result;
};

console.log("test1: ", handleUrlContact("/acm", "/bb"));
console.log("test2: ", handleUrlContact("/abc/", "/bcd"));
console.log("test3: ", handleUrlContact("/acd", "bef"));
console.log("test4: ", handleUrlContact("", ""));
