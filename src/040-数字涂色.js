/**
 * todo 数字涂色
 * @date 2024/1/2 - 12:19:35
 * @author Holy-Mortal
 */

/**
 * 需求
 *    疫情过后，希望小学终于又重新开学了，三年二班开学第一天的任务是将后面的黑板报重新制作。
 *    黑板上已经写上了N个正整数，同学们需要给这每个数分别上一种颜色。
 *    为了让黑板报既美观又有学习意义，老师要求同种颜色的所有数都可以被这种颜色中最小的那个数整除
 *    现在请你帮帮小朋友们，算算最少需要多少种颜色才能给这N个数进行上色
 * 输入描述
 *    第一个参数：正整数N，N取值范围[1, 100]
 *    第二个参数: 黑板上各正整数的值组成的数组，各个元素范围[1, 100]
 * 输出描述
 *    输出只有一个整数，为最少需要的颜色种数
 * 示例
 *    输入: (3, [2, 4, 6]) => 输出: 1, 所有数都能被2整除
 *    输入: (4, [2, 3, 4, 9]) => 输出: 2, 2与4涂一种颜色，4能被2整除；3与9涂另一种颜色，9能被3整除。不能4个数涂同一个颜色，因为3与9不能被2整除。所以最少的颜色是两种
 */
const handleDraeNumber = (count, arr) => {
  /** 校验输入参数 */
  if (!count || !arr) {
    return console.error("参数缺失，请核实！");
  }
  if (!determineInteger(count)) {
    return console.error("count 必须为整数！");
  }
  if (count < 1 || count > 100) {
    return console.error("count 取值范围[1, 100]");
  }
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是数组");
  }
  if (arr.length < 1 || arr.length > 100) {
    return console.error("arr 长度范围[1, 100]");
  }
  if (count !== arr.length) {
    return console.error("count 必须和 arr 长度保持一致！");
  }
  const flag = arr.every(
    (item) => determineInteger(item) && item >= 1 && item <= 100
  );
  if (!flag) {
    return console.error("数组中每一项范围【1,100】整数");
  }

  /** 核心代码区域 */
  const map = new Map();
  arr = arr.sort((a, b) => a - b);
  let maxValue = JSON.parse(JSON.stringify(arr))[count - 1];
  for (let i = 2; i <= maxValue; i++) {
    for (let j = 0; j < arr.length; j++) {
      if (arr[j] % i === 0) {
        if (map.has(i)) {
          let temp = map.get(i);
          temp.push(arr[j]);
          map.set(i, temp);
        } else {
          map.set(i, [arr[j]]);
        }
        arr.splice(j, 1);
        j--;
      }
    }
  }
  console.log(map);
  return map.size;
};

console.log("test1: ", handleDraeNumber(3, [2, 4, 6]));
console.log("test2: ", handleDraeNumber(4, [2, 3, 4, 9]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
