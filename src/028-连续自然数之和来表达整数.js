/**
 * todo 连续自然数之和来表达整数
 * @date 2023/12/30 - 13:46:37
 * @author Holy-Mortal
 */

/**
 * 需求
 *    一个整数可以由连续的自然数之和来表示。
 *    给定一个整数，计算该整数有几种连续自然数之和的表达式，且打印出每种表达式
 * 输入描述
 *    一个目标整数T (1 <=T<= 1000)
 * 输出描述
 *    该整数的所有表达式和表达式的个数。如果有多种表达式，输出要求为：
 *    1.自然数个数最少的表达式优先输出
 *    2.每个表达式中按自然数递增的顺序输出，具体的格式参见样例。
 *    在每个测试数据结束时，输出一行”Result:X”，其中X是最终的表达式个数
 * 示例
 *    输入: (9) => 输出: ["9", "45", "234"]
 *    输入: (10) => 输出: ["10", "1234"]
 */
const handleTranslateInteger = (num) => {
  /** 校验输入参数 */
  if (!determineInteger(num)) {
    return console.error("num 必须是一个整数！");
  }
  if (num < 1 || num > 1000) {
    return console.error("num 取值范围为[1, 1000]");
  }

  /** 核心代码区域 */
  let result = [];
  for (let i = 1; i <= Math.ceil(Math.sqrt(num)); i++) {
    const first = Math.max(Math.ceil(num / i - i / 2), 1);
    if (first * i + (i * (i - 1)) / 2 === num) {
      result.push(new Array(i).fill(first).map((it, i) => it + i));
    }
  }
  return result;
};

console.log("test1: ", handleTranslateInteger(9));
console.log("test2: ", handleTranslateInteger(10));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
