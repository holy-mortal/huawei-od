/**
 * todo 一种字符串压缩表示的解压
 * @date 2024/1/4 - 10:46:17
 * @author Holy-Mortal
 */

/**
 * 需求
 *    有一种简易压缩算法：针对全部由小写英文字母组成的字符串，
 *    将其中连续超过两个相同字母的部分压缩为连续个数加该字母，其他部分保持原样不变。
 *    例如：
 *        字符串“aaabbccccd”经过压缩成为字符串“3abb4cd”。
 *    请您编写解压函数，根据输入的字符串，判断其是否为合法压缩过的字符串，
 *    若输入合法则输出解压缩后的字符串，否则输出字符串“!error”来报告错误
 * 输入描述
 *    只包含数字和小写字母的字符串，长度不超过 100
 * 输出描述
 *    若判断输入为合法的经过压缩后的字符串，则输出压缩前的字符串；若输入不合法，则输出字符串“!error”
 * 示例
 *    输入: ("4dff") => 输出: "ddddff", 4d扩展为dddd，故解压后的字符串为ddddff
 *    输入: ("2dff") => 输出: "!error", 两个d不需要压缩，故输入不合法
 *    输入: ("4d@A") => 输出: "!error", 全部由小写英文字母组成的字符串压缩后不会出现特殊字符@和大写字母A，故输入不合法
 */
const handleDeCompression = (str) => {
  /** 校验输入参数 */
  if (typeof str !== "string" || str === "") {
    return "!error";
  }
  if (str.length < 0 || str.length > 100) {
    return "!error";
  }
  const rule = /^[0-9a-z]+$/;
  if (!rule.test(str)) {
    return "!error";
  }

  /** 核心代码区域 */
  let count = "";
  let result = [];
  for (let i = 0; i < str.length; i++) {
    let prev = str[i - 1] || "";
    let curr = str[i];
    let next = str[i + 1] || "";
    if (curr === prev && curr === next) {
      return "!error";
    }
    let char = Number(str[i]);
    if (Number.isNaN(char)) {
      if (Number(count) <= 2 && Number(count) > 0) {
        return "!error";
      }
      let temp = "".padEnd(Number(count) || 1, str[i]);
      result.push(temp);
      count = "";
    } else {
      count += str[i];
    }
  }
  return result.join("");
};
console.log("test1: ", handleDeCompression("4dff"));
console.log("test2: ", handleDeCompression("2dff"));
console.log("test3: ", handleDeCompression("4d@A"));
