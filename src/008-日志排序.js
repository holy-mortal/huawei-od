/**
 * todo 日志排序
 * @date 2023/12/19 - 16:51:29
 * @author Holy-Mortal
 */

/**
 * 需求
 *    运维工程师采集到某产品现网运行一天产生的日志N条，现需根据日志时间按时间先后顺序对日志进行排序
 *    日志格式：H:M:S.N
 *        H表示小时(0-23)，M表示分钟(0-59)，S表示秒(0-59)，N表示毫秒(0-999)
 *        时间可能并没有补齐，也就是说: 01:01:01.001，也可能表示为1:1:1.1
 * 输入描述
 *    第一个参数 N 表示日志条数，且 N 为整数，范围：1 <= N <= 100000
 *    第二个参数，表示日期数组
 * 输出描述
 *    按时间升序排序之后的时间，如果有两个时间表示的时间相同，则保持输入顺序
 * 示例
 *    输入: (2, ["01:41:8.9", "1:1:09.211"]) => 输出: ["1:1:09.211", "01:41:8.9"]
 *    输入: (3, ["23:41:08.023", "1:1:09.211", "08:01:22.0"]) => 输出: ["1:1:09.211", "08:01:22.0", "23:41:08.023"]
 *    输入: (2, ["22:41:08.023", "22:41:08.23"]) => 输出: ["22:41:08.023", "22:41:08.23"]
 */
const handleDateOrder = (count, dateArr) => {
  /** 校验输入参数 */
  if (!count || !dateArr) {
    return console.error("参数缺失，请核实！");
  }
  if (!determineInteger(count)) {
    return console.error("count 必须是整数");
  }
  if (count < 1 || count > 100000) {
    return console.error("count 取值范围：1 <= count <= 100000");
  }
  if (!Array.isArray(dateArr)) {
    return console.error("dateArr 必须是一个数组类型！");
  }
  if (count !== dateArr.length) {
    return console.error("count 必须等于 dateArr 数组的长度！");
  }
  const flag = dateArr.every((item) => typeof item === "string");
  if (!flag) {
    return console.error("dateArr 数组中每一项必须是字符串类型！");
  }

  /** 核心代码区域 */
  const newArr = dateArr
    .map((item) => {
      const frag = item.split(".");
      const h = Number(frag[0].split(":")[0]);
      const m = Number(frag[0].split(":")[1]);
      const s = Number(frag[0].split(":")[2]);
      const ms = Number(frag[1]);
      return {
        key: item,
        value: [h, m, s, ms],
      };
    })
    .sort((a, b) => {
      const va = a.value;
      const vb = b.value;
      if (va[0] === vb[0]) {
        if (va[1] === vb[1]) {
          if (va[2] === vb[2]) {
            return va[3] - vb[3];
          } else {
            return va[2] - vb[2];
          }
        } else {
          return va[1] - vb[2];
        }
      } else {
        return va[0] - vb[0];
      }
    });
  return newArr.map((item) => item.key);
};

console.log("test1: ", handleDateOrder(2, ["01:41:8.9", "1:1:09.211"]));
console.log(
  "test2: ",
  handleDateOrder(3, ["23:41:08.023", "1:1:09.211", "08:01:22.0"])
);
console.log("test3: ", handleDateOrder(2, ["22:41:08.023", "22:41:08.23"]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
