/**
 * todo 工号不够用处理办法
 * @date 2023/12/30 - 13:07:12
 * @author Holy-Mortal
 */

/**
 * 需求
 *    3020年，空间通信集团的员工人数突破20亿人，即将遇到现有工号不够用的窘境
 *    现在，请你负责调研新工号系统。继承历史传统，新的工号系统由小写英文字母（a-z）和数字（0-9）两部分构成
 *    新工号由一段英文字母开头，之后跟随一段数字，比如"aaahw0001","a12345","abcd1","a00"
 *    注意新工号不能全为字母或者数字,允许数字部分有前导0或者全为0
 *    但是过长的工号会增加同事们的记忆成本，现在给出新工号至少需要分配的人数X和
 *    新工号中字母的长度Y，求新工号中数字的最短长度Z
 * 输入描述
 *    X、Y为非负整数，0 < X <= 2^50 - 1, 0 < Y <= 5
 * 输出描述
 *    输出新工号中数字的最短长度Z
 * 示例
 *    输入: (260, 1) => 输出: 1
 *    输入: (26, 1) => 输出: 1
 *    输入: (2600, 1) => 输出: 2
 */
const handleJobNumber = (X, Y) => {
  /** 校验输入参数 */
  if (typeof X !== "number" || typeof Y !== "number") {
    return console.error("X、Y 必须是数字！");
  }
  if (X <= 0 || X > Math.pow(2, 50) - 1 || Y <= 0 || Y > 5) {
    return console.error("X、Y为非负整数，0 < X <= 2^50 - 1, 0 < Y <= 5");
  }
  // const rule = /^[a-z]+[0-9]+$/;
  /** 核心代码区域 */
  let temp = X / Math.pow(26, Y);
  let result = 0;
  while (Math.floor(temp / 10) > 0) {
    temp = temp / 10;
    result++;
  }
  return result === 0 ? 1 : result;
};
console.log("test1: ", handleJobNumber(260, 1));
console.log("test2: ", handleJobNumber(26, 1));
console.log("test3: ", handleJobNumber(2600, 1));
console.log("test4: ", handleJobNumber(260000, 1));
