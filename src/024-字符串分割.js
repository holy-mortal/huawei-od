/**
 * todo 字符串分割
 * @date 2023/12/29 - 15:38:49
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定一个非空字符串S，其被N个‘-’分隔成N+1的子串
 *    给定正整数K，要求除第一个子串外，其余的子串每K个字符组成新的子串，并用‘-’分隔
 *    对于新组成的每一个子串，如果它含有的小写字母比大写字母多，则将这个子串的所有大写字母转换为小写字母；
 *    反之，如果它含有的大写字母比小写字母多，则将这个子串的所有小写字母转换为大写字母；
 *    大小写字母的数量相等时，不做转换
 * 输入描述
 *    第一个参数为 K，第二个参数为字符串 S
 * 输出描述
 *    输出转换后的字符串
 * 示例
 *    输入: (3, "12abc-abCABc-4aB@") => 输出: "12abc-abc-ABC-4aB-@"
 *    输入: (12, "12abc-abCABc-4aB@") => 输出: "12abc-abCABc4aB@"
 */
const handleStringSplit = (count, str) => {
  /** 校验输入参数 */
  if (!count || !str) {
    return console.error("参数缺失，请核实！");
  }
  if (!determineInteger(count)) {
    return console.error("count 必须是一个整数！");
  }
  if (count <= 0) {
    return console.error("count 必须是一个正数！");
  }
  if (typeof str !== "string") {
    return console.error("str 必须是一个字符串！");
  }
  if (!str.includes("-")) {
    return console.error("str 必须至少包含一个 - ");
  }

  /** 核心代码区域 */
  let arr = str.split("-");
  const firstStr = arr.shift();
  let newArr = [];
  let newStr = arr.join("");
  let i = 0;
  while (i < Math.ceil(newStr.length / count)) {
    newArr.push(newStr.substring(i * count, count * (i + 1)));
    i++;
  }
  for (let i = 0; i < newArr.length; i++) {
    let str = newArr[i];
    let arr = [0, 0];
    for (let j = 0; j < str.length; j++) {
      if (
        str[j].charCodeAt(0) >= "a".charCodeAt(0) &&
        str[j].charCodeAt(0) <= "z".charCodeAt(0)
      ) {
        arr[0]++;
      }
      if (
        str[j].charCodeAt(0) >= "A".charCodeAt(0) &&
        str[j].charCodeAt(0) <= "Z".charCodeAt(0)
      ) {
        arr[1]++;
      }
    }
    if (arr[0] > arr[1]) {
      newArr[i] = newArr[i].toLowerCase();
    }
    if (arr[0] < arr[1]) {
      newArr[i] = newArr[i].toUpperCase();
    }
  }
  newArr.unshift(firstStr);
  return newArr.join("-");
};

console.log("test1: ", handleStringSplit(3, "12abc-abCABc-4aB@"));
console.log("test2: ", handleStringSplit(12, "12abc-abCABc-4aB@"));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
