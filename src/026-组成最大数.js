/**
 * todo 组成最大数
 * @date 2023/12/29 - 18:18:48
 * @author Holy-Mortal
 */

/**
 * 需求
 *    小组中每位都有一张卡片，卡片上是6位内的正整数，将卡片连起来可以组成多种数字，计算组成的最大数字
 * 输入描述
 *    多个正整数字符串组成的数组，长度不大于 25
 * 输出描述
 *    最大的数字字符串
 * 示例
 *    输入: ([22, 221]) => 输出: "22221"
 *    输入: ([4589, 101, 41425, 9999]) => 输出: "9999458941425101"
 */
const handleMakeMaxValue = (arr) => {
  /** 校验输入参数 */
  if (!arr) {
    return console.error("参数缺失，请核实！");
  }
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  const flag = arr.every((item) => {
    return determineInteger(item) && item >= 0 && item <= 999999;
  });
  if (!flag) {
    return console.error("数组中每一项都是数字，范围在[0, 999999]");
  }

  /** 核心代码区域 */
  const result = arr
    .sort((a, b) => {
      const strA = a.toString().padEnd(6, "9");
      const strB = b.toString().padEnd(6, "9");
      return Number(strB) - Number(strA);
    })
    .join("");
  return result;
};

console.log("test1: ", handleMakeMaxValue([22, 221]));
console.log("test2: ", handleMakeMaxValue([4589, 101, 41425, 9999]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
