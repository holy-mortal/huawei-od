/**
 * todo 字符统计及重排
 * @date 2023/12/19 - 15:02:25
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给出一个仅包含字母的字符串，不包含空格
 *    统计字符串中各个字母（区分大小写）出现的次数，并按照字母出现次数从大到小的顺序输出各个字母及其出现次数
 *    如果次数相同，按照自然顺序进行排序，且小写字母在大写字母之前
 * 输入描述
 *    输入一个仅包含字母的字符串
 * 输出描述
 *    按照字母出现次数从大到小的顺序输出各个字母和字母次数
 *    用英文分号分隔，注意末尾的分号；字母和次数间用英文冒号分隔
 * 示例
 *    输入: ("xyxyXX") => 输出: x:2,y:2,X:2; 每个字符出现的个数都是2，故x排在y之前，而小写字母x在X之前
 *    输入: ("abababb") => 输出: b:4,a:3; 每个字符出现的个数都是2，故x排在y之前，而小写字母x在X之前
 */
const handleCharStatisticsReorder = (str) => {
  /** 校验输入参数 */
  if (!str || typeof str !== "string") {
    return console.error("str 必须是字符串类型，且不为空！");
  }
  let rules = /^[a-zA-Z]+$/;
  if (!rules.test(str)) {
    return console.error("str 字符串仅包含字母！");
  }
  let map = new Map();
  for (let i = 0; i < str.length; i++) {
    const charCode = str.charCodeAt(i);
    if (map.has(charCode)) {
      map.set(charCode, map.get(charCode) + 1);
    } else {
      map.set(charCode, 1);
    }
  }
  let arr = Array.from(map).sort((a, b) => {
    if (a[1] === b[1]) {
      if ((a[0] < 97 && b[0] < 97) || (a[0] >= 97 && b[0] >= 97)) {
        return a[0] - b[0];
      }
      return b[0] - a[0];
    } else {
      return b[1] - a[1];
    }
  });
  let result = "";
  arr.forEach((item, i) => {
    const key = String.fromCharCode(item[0]);
    const value = item[1];
    if (i === arr.length - 1) {
      result += `${key}:${value};`;
    } else {
      result += `${key}:${value},`;
    }
  });
  return result;
};

console.log("test1: ", handleCharStatisticsReorder("xyxyXX"));
console.log("test2: ", handleCharStatisticsReorder("abababb"));
