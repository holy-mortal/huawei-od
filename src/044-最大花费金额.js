/**
 * todo 最大花费金额
 * @date 2024/1/2 - 17:00:47
 * @author Holy-Mortal
 */

/**
 * 需求
 *    双十一众多商品进行打折销售，小明想购买自己心仪的一些物品，但由于受购买资金限制，
 *    所以他决定从众多心仪商品中购买三件，而且想尽可能的花完资金，
 *    现在请你设计一个程序帮助小明计算尽可能花费的最大资金数额
 * 输入描述
 *    第一个参数：记录各个物品单价的整数型数组，长度范围[0, 100]，单价范围[0, 1000)
 *    第二个参数：购买资金的额度，取值范围 [0, 100000)
 * 输出描述
 *    输出为满足上述条件的最大花费额度
 * 注意
 *    如果不存在满足上述条件的商品，请返回-1
 * 示例
 *    输入: ([23, 26, 36, 27], 78) => 输出: 76, 金额23、26和27相加得到76，而且最接近且小于输入金额78
 *    输入: ([23, 30, 40], 26) => 输出: -1, 因为输入的商品，无法组合出来满足三件之和小于26.故返回-1
 */
const handleMaxFee = (arr, total) => {
  /** 校验输入参数 */
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  if (arr.length < 0 || arr.length > 100) {
    return console.error("arr 长度取值范围 [0,100");
  }
  const flag = arr.every(
    (item) => determineInteger(item) && item >= 0 && item < 1000
  );
  if (!flag) {
    return console.error("数组中每一项取值范围[0, 1000)");
  }
  if (total < 0 || total >= 100000) {
    return console.error("total 取值范围为[0, 100000)");
  }

  /** 核心代码区域 */
  function getCombination(arr, m) {
    let result = [];
    arr.sort();
    function permute(combination, remaining) {
      if (combination.length === m) {
        result.push(combination);
        return;
      }

      for (let i = 0; i < remaining.length; i++) {
        let restArr = [...remaining.slice(0, i), ...remaining.slice(i + 1)];
        permute([...combination, remaining[i]], restArr);
      }
    }
    permute([], arr);
    return result;
  }
  let tempRes = getCombination(arr, 3);
  /** 去重 */
  tempRes = Array.from(
    new Set(tempRes.map((item) => item.sort() && item.join("-")))
  ).map((item) => item.split("-").map((item) => Number(item)));

  let result = -1;
  for (let i = 0; i < tempRes.length; i++) {
    const item = tempRes[i];
    const calc = item[0] + item[1] + item[2];
    if (total >= calc) {
      result = Math.max(result, calc);
    }
  }
  return result;
};

console.log("test1: ", handleMaxFee([23, 26, 36, 27], 78));
console.log("test2: ", handleMaxFee([23, 30, 40], 26));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
