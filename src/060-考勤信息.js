/**
 * todo 考勤信息
 * @date 2024/1/5 - 18:57:28
 * @author Holy-Mortal
 */

/**
 * 需求
 *    公司用一个字符串来表示员工的出勤信息：
 *    absent：    缺勤
 *    late：      迟到
 *    leaveearly：早退
 *    present：   正常上班
 *    现需根据员工出勤信息，判断本次是否能获得出勤奖，
 *    能获得出勤奖的条件如下：
 *        缺勤不超过一次；
 *        没有连续的迟到/早退；
 *        任意连续7次考勤，缺勤/迟到/早退不超过3次
 * 输入描述
 *    第一个参数：总条数，记录条数 >= 1
 *    第二个参数：用户的考勤数据字符串组成的数组，；每个输入字符串长度 < 10000；不存在非法输入
 * 输出描述
 *    根据考勤数据字符串，如果能得到考勤奖，输出"true"；否则输出"false"
 * 示例
 *    输入: (2, ["present", "present present"]) => 输出: [true, true]
 *    输入: (2, ["present", "present absent present present leaveearly present absent"]) => 输出: [true, true]
 */
const handleCheckWorkAttendance = (count, arr) => {
  /** 校验输入参数 */
  if (!determineInteger(count)) {
    return console.error("count 必须是一个整数！");
  }
  if (count < 1) {
    return console.error("count 必须大于等于1");
  }
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  if (count !== arr.length) {
    return console.error("count 必须与 arr 长度保持一致！");
  }
  const example = {
    present: 1, // 正常上班
    late: 2, // 迟到
    leaveearly: 3, // 早退
    absent: 4, // 缺勤
  };
  arr = arr.map((item) => item.split(" "));
  const flag = arr.every((item) =>
    item.every((it) => Object.keys(example).includes(it))
  );
  if (!flag) {
    return console.error("arr 数组中每一项必须满足要求");
  }

  /** 核心代码区域 */
  arr = arr.map((item) => item.map((it) => example[it]).join(""));
  const result = new Array(count).fill(true);
  for (let i = 0; i < arr.length; i++) {
    const item = arr[i];
    let arr1 = item.match(/4/g) || [];
    let arr2 = item.match(/(22|33|23|32)/g) || [];
    if (arr1.length > 1 || arr2.length > 0) {
      result[i] = false;
    }
    if (item.length >= 7) {
      for (let j = 0; j <= item.length - 7; j++) {
        let arr3 = item.substring(i, i + 7).match(/[2-4]/g) || [];
        if (arr3.length > 3) {
          result[i] = false;
        }
      }
    } else {
      let arr4 = item.match(/[2-4]/g) || [];
      if (arr4.length > 3) {
        result[i] = false;
      }
    }
  }
  return result;
};

console.log(
  "test1: ",
  handleCheckWorkAttendance(2, ["present", "present present"])
);
console.log(
  "test1: ",
  handleCheckWorkAttendance(2, [
    "present",
    "present absent present present leaveearly present absent",
  ])
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
