/**
 * todo 玩牌高手
 * @date 2024/1/5 - 14:08:18
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定一个长度为n的整型数组，表示一个选手在n轮内可选择的牌面分数。
 *    选手基于规则选牌，请计算所有轮结束后其可以获得的最高总分数。
 *    选择规则如下：
 *        1、在每轮里选手可以选择获取该轮牌面，则其总分数加上该轮牌面分数，为其新的总分数。
 *        2、选手也可不选择本轮牌面直接跳到下一轮，此时将当前总分数还原为3轮前的总分数，
 *            若当前轮次小于等于3（即在第1、2、3轮选择跳过轮次），则总分数置为0。
 *        3、选手的初始总分数为0，且必须依次参加每一轮。
 * 输入描述
 *    参数：每一轮牌面分数形成的数组， 每一轮取值范围 [-100, 100]，轮数范围 [1, 20]
 * 输出描述
 *    所有轮结束后选手获得的最高总分数
 * 示例
 *    输入: ([1, -5, -6, 4, 3, 6, -2]) => 输出: 11
 * 说明
 *    总共有7轮牌面。
 *    第一轮选择该轮牌面，总分数为1。
 *    第二轮不选择该轮牌面，总分数还原为0。
 *    第三轮不选择该轮牌面，总分数还原为0。
 *    第四轮选择该轮牌面，总分数为4。
 *    第五轮选择该轮牌面，总分数为7。
 *    第六轮选择该轮牌面，总分数为13。
 *    第七轮如果不选择该轮牌面，则总分数还原到3轮1前分数，即第四轮的总分数4，如果选择该轮牌面，总分数为11，所以选择该轮牌面。
 *    因此，最终的最高总分为11。
 */
const handlePlayCards = (arr) => {
  /** 校验输入参数 */
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  if (arr.length < 1 || arr.length > 20) {
    return console.error("arr 长度范围[1, 20]");
  }
  const flag = arr.every(
    (item) => determineInteger(item) && item >= -100 && item <= 100
  );
  if (!flag) {
    return console.error("arr 数组中每一项取值范围[-100, 100]");
  }

  /** 核心代码区域 */
  let count = 0;
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    const item = arr[i];
    if (item >= 0) {
      count += item;
    } else {
      if (i <= 2) {
        count = Math.max(count + item, 0);
      } else {
        count = Math.max(count + item, result[i - 3]);
      }
    }
    result[i] = count;
  }
  return result[result.length - 1];
};

console.log("test: ", handlePlayCards([1, -5, -6, 4, 3, 6, -2]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
