/**
 * todo 字符串筛选排序
 * @date 2023/12/18 - 12:54:26
 * @author Holy-Mortal
 */

/**
 * 需求
 *    输入一个由n个大小写字母组成的字符串，按照Ascii码值从小到大的排序规则
 *    查找字符串中第k个最小ascii码值的字母（k>=1），输出该字母所在字符串的位置索引(字符串的第一个字符位置索引为0）。
 *    k如果大于字符串长度，则输出最大ascii值的字母所在字符串的位置索引，如果有重复的字母，则输出字母的最小位置索引。
 * 输入描述
 *    第一个参数输入一个由大小写字母组成的字符串
 *    第二个参数输入k，k必须大于0，k可以大于输入字符串的长度
 * 输出描述
 *    输出字符串中第k个最小ascii码值的字母所在字符串的位置索引。
 *    k如果大于字符串长度，则输出最大ascii值的字母所在字符串的位置索引
 *    如果第k个最小ascii码值的字母存在重复，则输出该字母的最小位置索引
 * 示例
 *    输入: (AbCdeFG, 3) => 输出: 5, 根据ascii码值排序，第3个最小ascii码值的字母为F，F在字符串中的位置索引为5（0为字符串的第一个字母位置索引）
 *    输入: (fAdDAkBbBq, 4) => 输出: 6, 根据ascii码值排序，前4个字母为AABB ，由于B重复，则只取B的（第一个）最小位置索引6 ，而不是第二个B的位置索引8
 */
const handleStringFilterSort = (str, k) => {
  /** 校验输入参数 */
  if (!str || !k) {
    return console.error("参数缺失，请核实！");
  }
  if (typeof str !== "string") {
    return console.error("str 必须为字符串类型！");
  }
  const rules = /^[a-zA-Z]+$/;
  if (!rules.test(str)) {
    return console.error("str 中只包含大小写字母！");
  }
  if (!determineInteger(k)) {
    return console.error("k 必须为整数！");
  }
  if (k < 1) {
    return console.error("k 必须大于0!");
  }

  /** 核心代码区域 */
  const arr = Array.from(new Array(str.length))
    .map((item, i) =>
      // str.charCodeAt(index) 根据索引获取对应字符的 ASCII 码
      str.charCodeAt(i)
    )
    .sort((a, b) => a - b);
  // String.fromCharCode(ASCII码) 根据 ASCII 码获取字符
  let code =
    k <= str.length
      ? // 最小 ASCII 码
        String.fromCharCode(arr[k - 1])
      : // 最大 ASCII 码
        String.fromCharCode(arr[arr.length - 1]);
  // 获取字母对应的索引
  return str.indexOf(code);
};

console.log("test1: ", handleStringFilterSort("6AbCdeFG", 3));
console.log("test2: ", handleStringFilterSort("fAdDAkBbBq", 4));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
