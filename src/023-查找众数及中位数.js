/**
 * todo 查找众数及中位数
 * @date 2023/12/29 - 14:53:29
 * @author Holy-Mortal
 */

/**
 * 需求
 *    1.众数是指一组数据中出现次数量多的那个数，众数可以是多个
 *    2.中位数是指把一组数据从小到大排列，最中间的那个数，如果这组数据的个数是奇数
 *      那最中间那个就是中位数，如果这组数据的个数为偶数，那就把中间的两个数之和除以2，所得的结果就是中位数
 *    3.查找整型数组中元素的众数并组成一个新的数组，求新数组的中位数
 * 输入描述
 *    输入一个一维整型数组，数组大小取值范围 0<N<1000，数组中每个元素取值范围 0<E<1000
 * 输出描述
 *    输出众数组成的新数组的中位数
 * 示例
 *    输入: ([10,11,21,19,21,17,21,16,21,18,15]) => 输出: 21
 *    输入: ([2,1,5,4,3,3,9,2,7,4,6,2,15,4,2,4]) => 输出: 3
 *    输入: ([5,1,5,3,5,2,5,5,7,6,7,3,7,11,7,55,7,9,98,9,17,9,15,9,9,1,39]) => 输出: 7
 */
const handleFindModeMedian = (arr) => {
  /** 校验输入参数 */
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  if (arr.length <= 0 || arr.length >= 1000) {
    return console.error("arr 数组长度范围(0,1000)");
  }
  const flag = arr.every((item) => {
    return determineInteger(item) && item > 0 && item < 1000;
  });
  if (!flag) {
    return console.error("arr 每一项为数字，且范围(0, 1000)");
  }

  /** 核心代码区域 */
  const map = new Map();
  for (let i = 0; i < arr.length; i++) {
    if (map.has(arr[i])) {
      map.set(arr[i], map.get(arr[i]) + 1);
    } else {
      map.set(arr[i], 1);
    }
  }
  let result = 0;
  arr = Array.from(map).sort((a, b) => b[1] - a[1]);
  arr = arr.filter((item) => item[1] === arr[0][1]).sort((a, b) => a[0] - b[0]);
  let len = arr.length;
  if (len % 2 === 0) {
    result = (arr[len / 2][0] + arr[len / 2 - 1][0]) / 2;
  } else {
    result = arr[(len - 1) / 2][0];
  }
  return result;
};

console.log(
  "test1: ",
  handleFindModeMedian([10, 11, 21, 19, 21, 17, 21, 16, 21, 18, 15])
);
console.log(
  "test2: ",
  handleFindModeMedian([2, 1, 5, 4, 3, 3, 9, 2, 7, 4, 6, 2, 15, 4, 2, 4])
);
console.log(
  "test3: ",
  handleFindModeMedian([
    5, 1, 5, 3, 5, 2, 5, 5, 7, 6, 7, 3, 7, 11, 7, 55, 7, 9, 98, 9, 17, 9, 15, 9,
    9, 1, 39,
  ])
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
