/**
 * todo 喊7次数重排
 * @date 2023/12/26 - 18:28:23
 * @author Holy-Mortal
 */

/**
 * 需求
 *    喊7是一个传统的聚会游戏，N个人围成一圈，按顺时针从1到N编号
 *    编号为1的人从1开始喊数，下一个人喊的数字为上一个人的数字加1，但是当将要喊出来的数字是7的倍数
 *    或者数字本身含有7的话，不能把这个数字直接喊出来，而是要喊"过"
 *    假定玩这个游戏的N个人都没有失误地在正确的时机喊了"过"，当喊到数字K时，可以统计每个人喊"过"的次数
 *    现给定一个长度为N的数组，存储了打乱顺序的每个人喊"过"的次数，请把它还原成正确的顺序
 *    即数组的第i个元素存储编号i的人喊"过"的次数
 * 输入描述
 *    输入一个长度为 N 的整数数组，K 不超过 200
 * 输出描述
 *    输出按索引+1为编号的整数数组
 * 示例
 *    输入: ([0, 1, 0]) => 输出: [1, 0, 0], 一共只有一次喊"过"，那只会发生在需要喊7时，按顺序，编号为1的人会遇到7，故输出1 0 0。注意，结束时的K不一定是7，也可以是8、9等，喊过的次数都是1 0 0。
 *    输入: ([0, 0, 0, 2, 1]) => 输出: [0, 2, 0, 1, 0], 一共有三次喊"过"，发生在7 14 17，按顺序，编号为2的人会遇到7 17，编号为4的人会遇到14，故输出0 2 0 1 0
 */
const handleArrSort = (arr) => {
  /** 校验输入参数 */
  if (!arr || !Array.isArray(arr)) {
    return console.error("传入参数必须是一个数组！");
  }
  const flag = arr.every((item) => determineInteger(item) && item >= 0);
  if (!flag) {
    return console.error("数组中每个元素必须是正整数");
  }

  /** 核心代码区域 */
  const len = arr.length;
  const temp = [];
  for (let i = 1; i < 200; i++) {
    if (i % 7 === 0 || i.toString().includes("7")) {
      temp.push(i);
    }
  }
  const index = arr.reduce((prev, curr) => (prev += curr), 0) - 1;
  const result = new Array(len).fill(0);
  for (let i = 1; i <= temp[index]; i++) {
    if (i % 7 === 0 || i.toString().includes("7")) {
      result[(i - 1) % len]++;
    }
  }
  return result;
};

console.log("test1: ", handleArrSort([0, 1, 0]));
console.log("test2: ", handleArrSort([0, 0, 0, 2, 1]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
