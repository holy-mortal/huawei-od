/**
 * todo 分班
 * @date 2024/1/4 - 12:12:49
 * @author Holy-Mortal
 */

/**
 * 需求
 *    幼儿园两个班的小朋友在排队时混在了一起，每位小朋友都知道自己是否与前面一位小朋友是否同班，
 *    请你帮忙把同班的小朋友找出来。
 *    小朋友的编号为整数，与前一位小朋友同班用Y表示，不同班用N表示。
 * 输入描述
 *    输入为空格分开的小朋友编号和是否同班标志。
 *    比如：6/N 2/Y 3/N 4/Y，表示共4位小朋友，2和6同班，3和2不同班，4和3同班。
 *    其中，小朋友总数不超过999，每个小朋友编号大于0，小于等于999。
 *    不考虑输入格式错误问题
 * 输出描述
 *    输出一个数组，每一个元素为一个班级内所有小朋友。且：
 *    1、编号需要按照大小升序排列，分班记录中第一个编号小的排在第一行。
 *    2、若只有一个班的小朋友，第二行为空行。
 *    3、若输入不符合要求，则直接输出字符串ERROR。
 * 示例
 *    输入: ([[1, "N"], [2, "Y"], [3, "N"], [4, "Y"]) => 输出: [[1, 2], [3, 4]]
 */
const handleDivideClass = (arr) => {
  /** 校验输入参数 */
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  const flag = arr.every(
    (item) => determineInteger(item[0]) && (item[1] === "Y" || item[1] === "N")
  );
  if (!flag) {
    return "ERROR";
  }

  /** 核心代码区域 */
  const setA = new Set();
  const setB = new Set();
  setA.add(arr[0][0]);
  for (let i = 1; i < arr.length; i++) {
    const item = arr[i];
    if (item[1] === "Y") {
      if (setA.has(arr[i - 1][0])) {
        setA.add(item[0]);
      } else {
        setB.add(item[0]);
      }
    } else if (item[1] === "N") {
      if (setA.has(arr[i - 1][0])) {
        setB.add(item[0]);
      } else {
        setA.add(item[0]);
      }
    }
  }
  return [
    [...setA].sort((a, b) => a - b),
    [...setB].sort((a, b) => a - b),
  ].sort((a, b) => a[0] - b[0]);
};

console.log(
  "test: ",
  handleDivideClass([
    [1, "N"],
    [2, "Y"],
    [3, "N"],
    [4, "Y"],
  ])
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
