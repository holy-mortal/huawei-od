/**
 * todo 敏感字段加密
 * @date 2023/12/30 - 17:51:22
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定一个由多个命令字组成的命令字符串：
 *    1、字符串长度小于等于127字节，只包含大小写字母，数字，下划线和偶数个双引号；
 *    2、命令字之间以一个或多个下划线_进行分割；
 *    3、可以通过两个双引号""来标识包含下划线_的命令字或空命令字（仅包含两个双引号的命令字），
 *       双引号不会在命令字内部出现；
 *    请对指定索引的敏感字段进行加密，替换为******（6个*），
 *    并删除命令字前后多余的下划线_。如果无法找到指定索引的命令字，输出字符串ERROR
 * 输入描述
 *
 */
const handleSensitiveFieldEncrypt = (index, str) => {
  /** 校验输入参数 */
  if (!determineInteger(index)) {
    return console.error("index 必须是一个整数！");
  }
  if (index < 0) {
    return console.error("index 取值范围 大于等于0");
  }
  const rule = /[0-9a-zA-Z_"]+/;
  if (!rule.test(str)) {
    return console.error("str 必须满足格式要求！");
  }

  /** 核心代码区域 */
  const tempArr = str.split('"');
  for (let i = 0; i < tempArr.length; i++) {
    if (i % 2 === 1) {
      if (tempArr[i].includes("_")) {
        tempArr[i] = tempArr[i].replace("_", "$");
      } else if (tempArr[i] === "") {
        tempArr[i] = '""';
      }
    }
  }
  let tempStr = tempArr.join("");
  if (tempStr.includes("__")) {
    tempStr = tempStr.replace("__", "_");
  }
  const newArr = tempStr.split("_");
  let result = "ERROR";
  if (index < newArr.length) {
    newArr[index] = "******";
    result = newArr
      .filter((item) => item)
      .join("_")
      .replace("$", "_");
  }
  return result;
};

console.log(
  "test1: ",
  handleSensitiveFieldEncrypt(1, "password__a12345678_timeout_100")
);
console.log(
  "test2: ",
  handleSensitiveFieldEncrypt(2, 'aaa_password_"a12_45678"_timeout__100_""_')
);

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
