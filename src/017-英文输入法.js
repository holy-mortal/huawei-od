/**
 * todo 英文输入法
 * @date 2023/12/28 - 10:26:55
 * @author Holy-Mortal
 */

/**
 * 需求
 *    主管期望你来实现英文输入法单词联想功能。需求如下：
 *        依据用户输入的单词前缀，从已输入的英文语句中联想出用户想输入的单词
 *    按字典序输出联想到的单词序列，如果联想不到，请输出用户输入的单词前缀。
 * 注意：
 *    1.  英文单词联想时，区分大小写
 *    2.  缩略形式如”don't”，判定为两个单词，”don”和”t”
 *    3.  输出的单词序列，不能有重复单词，且只能是英文单词，不能有标点符号
 * 输入描述
 *    第一个参数，一端包含单词和标点符号的字符串，长度范围(0, 10000]
 *    其中单词长度范围(0, 20]
 *    第二个参数，英文单词前缀范围(0, 20]
 * 输出描述
 *    输出符合要求的单词序列或单词前缀，存在多个时，以数组形式输出
 * 示例
 *    输入: ("I love you", "He") => 输出: "He"
 *    输入: ("The furthest distance in the world, Is not between life and death, But when I stand in front of you, Yet you don't know that I love you.", "f") => 输出: ["front", "furthest"]
 */
const handleAlphabeticInput = (str, pre) => {
  /** 校验输入参数 */
  if (!str || !pre) {
    return console.error("参数缺失，请核实！");
  }
  if (typeof str !== "string" || typeof pre !== "string") {
    return console.error("参数均为字符串类型！");
  }
  if (str.length > 10000 || pre.length > 20) {
    return console.error("str 长度范围(0, 10000]，pre 长度范围(0, 20]！");
  }
  const rule = /[,.':;?!]/;
  const arr = str.replace(rule, " ").split(" ");
  const flag = arr.every(
    (item) => typeof item === "string" && item.length <= 20
  );
  if (!flag) {
    return console.error("每个单词长度不超过20");
  }

  /** 核心代码区域 */
  const res = arr.filter((item) => item.startsWith(pre));
  return res.length > 0 ? res.sort((a, b) => a.localeCompare(b)) : pre;
};

console.log("test1: ", handleAlphabeticInput("I love you", "He"));
console.log(
  "test2: ",
  handleAlphabeticInput(
    "The furthest distance in the world, Is not between life and death,But when I stand in front of you, Yet you don't know that I love you.",
    "f"
  )
);
