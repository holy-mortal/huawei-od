/**
 * todo 找车位
 * @date 2024/1/2 - 15:48:14
 * @author Holy-Mortal
 */

/**
 * 需求
 *    停车场有一横排车位，0代表没有停车，1代表有车。至少停了一辆车在车位上，也至少有一个空位没有停车
 *    为了防剐蹭，需为停车人找到一个车位，使得距停车人的车最近的车辆的距离是最大的，返回此时的最大距离
 * 输入描述
 *    参数：只包含0、1的数组，长度范围 [0, 100]
 * 输出描述
 *    输出一个整数记录最大距离
 * 示例
 *    输入: ([1,0,0,0,0,1,0,0,1,0,1]) => 输出: 2
 */
const handleFindCarPosition = (arr) => {
  /** 校验输入参数 */
  if (!Array.isArray(arr)) {
    return console.error("arr 必须是一个数组！");
  }
  if (arr.length < 1 || arr.length > 100) {
    return console.error("arr 取值范围[1, 100]");
  }
  const flag = arr.every((item) => item === 1 || item === 0);
  if (!flag) {
    return console.error("arr 数组中每一项只能是0/1");
  }

  /** 核心代码区域 */
  let temp = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === 1) {
      temp.push(i + 1);
    }
  }
  const map = new Map();
  for (let i = 1; i < temp.length; i++) {
    let key = temp[i] - temp[i - 1];
    if (map.has(key)) {
      let tempArr = map.get(key);
      tempArr.push(temp[i - 1]);
      map.set(key, tempArr);
    } else {
      map.set(key, [temp[i - 1]]);
    }
  }
  const result = [0, []];
  const tempRes = Array.from(map).sort((a, b) => b[0] - a[0])[0];
  result[0] = Math.floor(tempRes[0] / 2);
  for (let i = 0; i < tempRes[1].length; i++) {
    result[1].push(tempRes[1][i] + result[0]);
  }
  if (tempRes[0] % 2 !== 0) {
    for (let i = 0; i < tempRes[1].length; i++) {
      result[1].push(tempRes[1][i] + result[0] + 1);
    }
  }
  result[1] = result[1].sort();
  return result;
};

console.log("test: ", handleFindCarPosition([1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1]));
