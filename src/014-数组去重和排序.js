/**
 * todo 数组去重和排序
 * @date 2023/12/26 - 17:54:34
 * @author Holy-Mortal
 */

/**
 * 需求
 *    给定一个乱序的数组，删除所有的重复元素，使得每个元素只出现一次，并且按照出现的次数从高到低进行排序，
 *    相同出现次数按照第一次出现顺序进行先后排序
 * 输入描述
 *    一个数组，长度不超过 100，各个元素值不超过100
 * 输出描述
 *    去重排序后的数组
 * 示例
 *    输入: ([1,3,3,3,2,4,4,4,5]) => 输出: [3, 4, 1, 2, 5]
 */
const handleUniqueSort = (arr) => {
  /** 校验输入参数 */
  if (!arr || !Array.isArray(arr)) {
    return console.error("传入参数必须是一个数组！");
  }
  if (arr.length > 100) {
    return console.error("数组长度不能超过100!");
  }
  const flag = arr.every((item) => determineInteger(item) && item <= 100);
  if (!flag) {
    return console.error("数组中每个元素必须是整数，且不大于100");
  }

  /** 核心代码区域 */
  const map = new Map();
  for (let i = 0; i < arr.length; i++) {
    if (map.has(arr[i])) {
      const count = map.get(arr[i]) + 1;
      map.set(arr[i], count);
    } else {
      map.set(arr[i], 1);
    }
  }
  return Array.from(map)
    .sort((a, b) => b[1] - a[1])
    .map((item) => item[0]);

  // const map = {};
  // for (let i = 0; i < arr.length; i++) {
  //   if (Object.hasOwnProperty.call(map, arr[i])) {
  //     map[arr[i]]++;
  //   } else {
  //     map[arr[i]] = 1;
  //   }
  // }
  // return Object.entries(map)
  //   .sort((a, b) => b[1] - a[1])
  //   .map((item) => Number(item[0]));
};

console.log("test: ", handleUniqueSort([1, 3, 3, 3, 2, 4, 4, 4, 5]));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
