/**
 * todo 求解连续数列
 * @date 2023/12/26 - 16:41:21
 * @author Holy-Mortal
 */

/**
 * 需求
 *    已知连续正整数数列{K}=K1,K2,K3...Ki的各个数相加之和为S，i=N (0<S<100000, 0<N<100000), 求此数列K。
 * 输入描述
 *    输入包含两个参数，1）连续正整数数列和S，2）数列里数的个数N。
 * 输出描述
 *    如果有解输出数列K，如果无解输出-1
 * 示例
 *    输入: (525, 6) => 输出: [85, 86, 87, 88, 89, 90]
 *    输入: (3, 5) => 输出: -1
 */
const handleContinuousArr = (sum, len) => {
  /** 校验输入参数 */
  if (sum === undefined || len === undefined) {
    return console.error("参数缺失，请核实！");
  }
  if (!determineInteger(sum) || !determineInteger(len)) {
    return console.error("sum、len 参数必须为整数");
  }
  if (sum <= 0 || sum >= 100000 || len <= 0 || len >= 100000) {
    return console.error("sum、len的取值范围为(0,100000)");
  }

  /** 核心代码区域 */
  let result = [];
  const forlen = Math.max(Math.ceil(sum / len), 1);
  for (let i = 1; i <= forlen; i++) {
    if (((i + (i + len - 1)) * len) / 2 === sum) {
      result.push(new Array(len).fill(i).map((it, i) => it + i));
      break;
    }
  }
  return result.length === 0 ? "-1" : result[0];
};

console.log("test1: ", handleContinuousArr(525, 6));
console.log("test2: ", handleContinuousArr(3, 5));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
