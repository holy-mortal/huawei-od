/**
 * todo 快递运输
 * @date 2023/12/30 - 17:07:25
 * @author Holy-Mortal
 */

/**
 * 需求
 *    一辆运送快递的货车，运送的快递均放在大小不等的长方体快递盒中，
 *    为了能够装载更多的快递，同时不能让货车超载，需要计算最多能装多少个快递
 *    注：快递的体积不受限制，快递数最多1000个，货车载重最大50000
 * 输入描述
 *    第一个参数：包含每个快递重量的数组
 *    第二个参数：货车载重量
 * 输出描述
 *    输出最多能装多少个快递
 * 示例
 *    输入: ([5, 10, 2, 11], 20) => 输出: 3
 */
const handleExpressTransportation = (arr, total) => {
  /** 校验输入参数 */
  if (!Array.isArray(arr) || typeof total !== "number") {
    return console.error("请核实参数！");
  }
  if (total < 0 || total > 50000) {
    return console.error("total 取值范围：[0, 50000]");
  }
  if (arr.length < 0 || arr.length > 1000) {
    return console.error("arr 长度最多1000");
  }
  const flag = arr.every((item) => typeof item === "number" && item >= 0);
  if (!flag) {
    return console.error("arr 数组中每一项均为数字，且大于等于0");
  }

  /** 核心代码区域 */
  const result = [];
  arr.sort((a, b) => a - b);
  let sum = 0;
  let i = 0;
  while (sum <= total) {
    sum += arr[i];
    if (sum <= total) {
      result.push(arr[i]);
    }
    i++;
  }
  return [result, result.length];
};

console.log("test: ", handleExpressTransportation([5, 10, 2, 11], 20));
