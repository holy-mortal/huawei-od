/**
 * todo 处理大数字相加
 * @date 2023/12/26 - 13:11:31
 * @author Holy-Mortal
 */

/**
 * 需求
 *    将两个整数数字型字符串相加
 */
const handleSum = (a, b) => {
  /** 校验输入参数 */
  if ((!a && a !== 0) || (!b && b !== 0)) {
    return console.error("参数缺失，请核实！");
  }
  if (typeof a !== "string" || typeof b !== "string") {
    return console.error("参数必须字符串！");
  }
  const rule = /^[0-9]+$/;
  if (!rule.test(a) || !rule.test(b)) {
    return console.error("参数必须为数字型字符串！");
  }
  if (!determineInteger(Number(a)) || !determineInteger(Number(b))) {
    return console.error("参数必须为整数型字符串！");
  }

  /** 核心代码区域 */
  const len = Math.max(a.length, b.length);
  a = a.padStart(len, "0");
  b = b.padStart(len, "0");
  /** 定义进位值 */
  let carry = 0;
  let result = "";
  for (let i = len - 1; i >= 0; i--) {
    const sum = +a[i] + +b[i] + carry;
    /** 当前位显示值 */
    const r = sum % 10;
    carry = Math.floor(sum / 10);
    result = r + result;
  }
  return result;
};

console.log("test: ", handleSum("6785656676", "76567746985986563876"));

/**
 * 判断是否为整数
 * @param {any} parameter 参数
 * @returns {boolean}
 */
function determineInteger(parameter) {
  if (typeof parameter !== "number") {
    console.error("The parameter must be a number!");
    return false;
  }
  if (!Number.isFinite(parameter)) {
    console.error("The parameter must be a finite number!");
    return false;
  }
  if (Number.isNaN(parameter)) {
    console.error("The parameter must not be a NaN!");
    return false;
  }
  if (!Number.isInteger(parameter)) {
    console.error("The parameter must be an integer!");
    return false;
  }
  return true;
}
