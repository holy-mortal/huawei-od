/**
 * todo 深拷贝
 * @date 2023/12/26 - 13:47:50
 * @author Holy-Mortal
 */

/**
 * 需求
 *    深拷贝数据
 */
const deepClone = (value) => {
  const cache = new WeakMap();
  function _deepClone(value) {
    if (value === null || typeof value !== "object") {
      return value;
    }
    if (value instanceof Date) {
      return new Date(value);
    }
    if (value instanceof RegExp) {
      return new RegExp(value);
    }
    if (cache.has(value)) {
      return cache.get(value);
    }
    const descriptors = Object.getOwnPropertyDescriptors(value);
    const prototypeof = Object.getPrototypeOf(value);
    const result = Object.create(prototypeof, descriptors);
    cache.set(value, result);
    for (const key of Reflect.ownKeys(value)) {
      result[key] = _deepClone(value[key]);
    }
    return result;
  }
  return _deepClone(value);
};

console.log(
  "test: ",
  deepClone([
    {
      time: new Date(),
      rule: new RegExp(),
      obj: {
        rule: "huihoihdfofd",
      },
      arr: [1, 2, 3],
    },
  ])
);

const deepCloneExample = (value) => {
  const cache = new WeakMap();
  function _deepClone(value) {
    if (value === null || typeof value !== "object") {
      return value;
    }
    if (value instanceof Date) {
      return new Date(value);
    }
    if (value instanceof RegExp) {
      return new RegExp(value);
    }
    if (cache.has(value)) {
      return cache.get(value);
    }
    const result = Array.isArray(value) ? [] : {};
    cache.set(value, result);
    for (const key in value) {
      if (Object.hasOwnProperty.call(value, key)) {
        result[key] = _deepClone(value[key]);
      }
    }
    return result;
  }
  return _deepClone(value);
};
