/**
 * todo 防抖和节流
 * @date 2023/12/26 - 14:36:16
 * @author Holy-Mortal
 */

/**
 * 需求
 *    封装一个防抖函数
 */
const debounce = (callback, duration = 500) => {
  let timerId;
  return function (...args) {
    clearTimeout(timerId);
    timerId = setTimeout(() => {
      callback.apply(this, args);
    }, duration);
  };
};

/**
 * 需求
 *    封装一个节流函数
 */
const throttle = (callback, duration = 500, immediate) => {
  if (immediate === undefined) {
    /** immediate 表示立即执行，不会多执行一次 */
    immediate = true;
  }
  if (immediate) {
    let timestamp;
    return function (...args) {
      if (!timestamp || Date.now() - timestamp >= duration) {
        callback.apply(this, args);
        timestamp = Date.now();
      }
    };
  } else {
    let timerId;
    return function (...args) {
      if (timerId) return;
      timerId = setTimeout(() => {
        callback.apply(this, args);
        timerId = null;
      }, duration);
    };
  }
};
