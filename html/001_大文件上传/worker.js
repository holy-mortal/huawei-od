import { createChunk } from "./createChunk.js";

onmessage = async (e) => {
  const {
    file,
    CHUNK_SIZE,
    startChunkIndex: start,
    endChunkIndex: end,
  } = e.data;
  console.log(file, CHUNK_SIZE, start, end);
  const promises = [];
  for (let i = start; i < end; i++) {
    promises.push(createChunk(file, i, CHUNK_SIZE));
  }
  const chunks = await Promise.all(promises);
  postMessage(chunks);
};
