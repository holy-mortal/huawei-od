import { createChunk } from "./createChunk.js";

// 定义切片大小
const CHUNK_SIZE = 1024 * 1024 * 1; // 5M

export async function cutFile1(file) {
  const chunkCount = Math.ceil(file.size / CHUNK_SIZE);
  const result = [];
  for (let i = 0; i < chunkCount; i++) {
    const chunk = await createChunk(file, i, CHUNK_SIZE);
    result.push(chunk);
  }
  return result;
}

// 并行版本
export async function cutFile2(file) {
  const chunkCount = Math.ceil(file.size / CHUNK_SIZE);
  const promises = [];
  for (let i = 0; i < chunkCount; i++) {
    promises.push(createChunk(file, i, CHUNK_SIZE));
  }
  const result = await Promise.all(promises);
  return result;
}

// 内核数
const THREAD_COUNT = navigator.hardwareConcurrency || 4;
console.log(THREAD_COUNT);
// 多线程版本
export function cutFile(file) {
  return new Promise((resolve) => {
    const chunkCount = Math.ceil(file.size / CHUNK_SIZE);
    const threadChunkCount = Math.ceil(chunkCount / THREAD_COUNT);
    const result = [];
    const finishCount = 0;
    for (let i = 0; i < threadChunkCount; i++) {
      // 创建一个线程，并分配任务
      const worker = new Worker("./worker.js", {
        type: "module",
      });
      const start = i * threadChunkCount;
      const end = (i + 1) * threadChunkCount;
      if (end > chunkCount) {
        end = chunkCount;
      }
      // 向线程中传递内容
      worker.postMessage({
        file,
        CHUNK_SIZE,
        startChunkIndex: start,
        endChunkIndex: end,
      });
      // 收到消息
      worker.onmessage = (e) => {
        for (let j = start; j < end; j++) {
          result[j] = e.data[j - start];
        }
        worker.terminate();
        finishCount++;
        if (finishCount === THREAD_COUNT) {
          resolve(result);
        }
      };
    }
  });
}
