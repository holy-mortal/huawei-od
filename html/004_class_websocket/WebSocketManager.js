export default class WebSocketManager {
  // 构造器
  constructor(url = null, userId = null, receiveMessageCallback = null) {
    // WebSocket 对象
    this.socket = null;
    // 心跳计时器
    this.pingTimeout = null;
    // 重连间隔，单位： 毫秒
    this.reconnectTimeout = 5000;
    // 最大重连尝试次数
    this.maxReconnectAttempts = 10;
    // 当前重连尝试次数
    this.reconnectAttempts = 0;
    // 用户ID（业务逻辑，根据自己业务需求调整）
    this.id = userId;
    // WebSocket 连接地址
    this.url = url;
    // 接收信息回调函数
    this.receiveMessageCallback = receiveMessageCallback;
  }

  // 初始化
  async start() {
    if (this.url && this.id) {
      // 连接 WebSocket
      this.connectWebSocket();
    } else {
      console.error("WebSocketManager errors: 请传入连接地址和用户id");
    }
  }

  // 创建 WebSocket 连接
  connectWebSocket() {
    // 通过 id 生成唯一值（服务端要求，具体根据自己业务去调整）
    let id = `${this.id}-${Math.random()}`;
    // 创建 WebSocket 对象
    this.socket = new WebSocket(this.url);
    console.log("socket: ", this.socket);
    // 处理连接打开事件
    this.socket.addEventListener("open", (e) => {
      // 启动心跳计时器
      this.startHeartbeat();
    });
    // 处理接收到消息事件
    this.socket.addEventListener("message", (e) => {
      this.receiveMessage(e);
    });
    // 处理连接关闭事件
    this.socket.addEventListener("close", (e) => {
      // 清除定时器
      clearTimeout(this.pingTimeout);
      clearTimeout(this.reconnectTimeout);
      // 尝试重连
      if (this.reconnectAttempts < this.maxReconnectAttempts) {
        this.reconnectAttempts++;
        this.reconnectTimeout = setTimeout(() => {
          this.connectWebSocket();
        }, this.reconnectTimeout);
      } else {
        // 重置重连次数
        this.reconnectAttempts = 0;
        console.error(
          "WebSocketManager errors: Max reconnect attempts reached. Unable to reconnect."
        );
      }
    });
    // 处理 WebSocket 错误事件
    this.socket.addEventListener("error", (e) => {
      console.error("WebSocketManager errors: ", e);
    });
  }

  /**
   * 启动心跳机制
   * 检测客户端是否在线，若客户端超过一定时间未发送消息，服务端会断开连接
   * 每隔一段时间发送心跳连接，避免长时间断开连接
   */
  startHeartbeat() {
    this.pingTimeout = setInterval(() => {
      // 每隔 10s 发送心跳消息
      this.sendMessage("ping");
    }, 10000);
  }

  /**
   * 发送消息
   * message：消息内容
   */
  sendMessage(message) {
    console.log(this.socket.readyState);
    if (this.socket.readyState === WebSocket.OPEN) {
      this.socket.send(message);
    } else {
      console.error(
        "WebSocketManager errors: WebSocket connection is not open. Unable to send message."
      );
    }
  }

  /**
   * 接收消息
   * 根据消息内容做不同处理
   */
  receiveMessage(e) {
    // 根据业务自行处理
    console.log("receiveMessage: ", e.data);
    this.receiveMessageCallback && this.receiveMessageCallback(e.data);
  }

  /**
   * 关闭连接
   * 退出登录、关闭连接
   * 清除定时器和当前重连尝试次数
   */
  closeWebSocket() {
    this.socket.close();
    // 清除定时器，重置重连次数
    clearTimeout(this.pingTimeout);
    clearTimeout(this.reconnectTimeout);
    this.reconnectAttempts = 0;
  }
}
