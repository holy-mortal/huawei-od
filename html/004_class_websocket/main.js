import WebSocketManager from "./WebSocketManager.js";

const receiveMessage = (res) => {
  // console.log("接收消息回调：", res);
};

const socketManager = new WebSocketManager(
  "ws://127.0.0.1:8080",
  "userid292992",
  receiveMessage
);
socketManager.start();
setInterval(() => {
  socketManager.sendMessage("Hello, WebSocket!");
}, 2000);

// const ws = new WebSocket("ws://127.0.0.1:8080");
// console.log("ws: ", ws);
// ws.onopen = () => {
//   console.log("ws成功！");
// };
