/**
 * todo: ReturnType<R>
 * 一个泛型工具类型，它用于获取函数类型T的返回值类型
 * 接受一个函数类型作为参数，并返回该函数的返回值类型
 * @date 2024/1/8 - 10:56:56
 * @author Holy-Mortal
 *
 * @typedef {DemoReturnType}
 */
type DemoReturnType = ReturnType<typeof ReturnTypeDemo>;

/**
 * ? 实现过程
 * @typedef {CustomReturnType}
 * @template {(...args: any) => any} T
 */
type CustomReturnType<T extends (...args: any) => any> = T extends (
  ...args: any
) => infer R
  ? R
  : never;

/**
 * 测试用例
 */
function ReturnTypeDemo(name: string, age: number): number {
  return 1;
}
type ReturnTypeTest001 = CustomReturnType<typeof ReturnTypeDemo>;
