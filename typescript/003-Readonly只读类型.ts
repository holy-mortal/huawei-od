/**
 * todo: Readonly<T>
 * 用于将给定类型的所有属性设置为只读
 * 创建一个新的类型，该类型具有与原始类型相同的属性，但是这些属性都是只读的，不能被修改
 * @date 2024/1/8 - 09:33:56
 * @author Holy-Mortal
 *
 * @typedef {DemoReadonly}
 */
type DemoReadonly = Readonly<ReadonlyDemo>;

/**
 * ? 实现过程
 * @typedef {CustomRequired}
 * @template T
 */
type CustomReadonly<T> = {
  /** readonly 只读类型 */
  readonly [P in keyof T]: T[P];
};

/**
 * 测试用例
 */
type ReadonlyDemo = {
  name: string;
  gender: string;
  age: number;
  exist: boolean;
};

type ReadonlyTest001 = CustomReadonly<ReadonlyDemo>;
