/**
 * todo Required<T>
 * 用于将给定类型的所有属性设置为必需的
 * 创建一个新的类型，该类型具有与原始类型相同的属性，但是这些属性都是必需的，不能省略
 * @date 2024/1/8 - 09:28:19
 * @author Holy-Mortal
 *
 * @typedef {DemoRequired}
 */
type DemoRequired = Required<RequiredDemo>;

/**
 * ? 实现过程
 * @typedef {CustomRequired}
 * @template T
 */
type CustomRequired<T> = {
  // -? 必选操作符
  [P in keyof T]-?: T[P];
};

/**
 * 测试用例
 */
type RequiredDemo = {
  name: string;
  gender: string;
  age: number;
  exist: boolean;
};
type RequiredTest001 = CustomRequired<RequiredDemo>;
