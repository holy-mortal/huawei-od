/**
 * todo: Lowercase<S extends string>
 * 用于将字符串类型的字母转换为小写
 * 接受一个字符串类型作为参数，并返回该字符串类型的小写版本
 * @date 2024/1/8 - 11:09:23
 * @author Holy-Mortal
 *
 * @typedef {DemoLowercase}
 */
type DemoLowercase = Lowercase<"Hello,Typescript">;

/**
 * ? 实现过程
 * @typedef {CustomLowercase}
 * @template {string} S
 */
type CustomLowercase<S extends string> = intrinsic;

/**
 * 测试用例
 */
type LowercaseTest001 = CustomLowercase<"Hello,Typescript">;
