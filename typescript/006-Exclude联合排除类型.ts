/**
 * todo: Exclude<T, U>
 * 用于从一个联合类型中排除指定的类型
 * 接受两个类型参数，第一个参数是要排除的类型，第二个参数是要从中排除类型的联合类型
 * @date 2024/1/8 - 10:05:19
 * @author Holy-Mortal
 *
 * @typedef {DemoExclude}
 */
type DemoExclude = Exclude<
  "name" | "age" | "gender" | "exist",
  "name" | "prop"
>;

/**
 * ? 实现过程
 * @typedef {CustomExclude}
 * @template T
 * @template U
 */
type CustomExclude<T, U> = T extends U ? never : T;

/**
 * 测试用例
 */
type ExcludeTest001 = CustomExclude<
  "name" | "age" | "gender" | "exist",
  "name" | "prop"
>;
