/**
 * todo: Extract<T, U>
 * 用于从一个联合类型中提取指定的类型
 * 受两个类型参数，第一个参数是要提取的类型，第二个参数是要从中提取类型的联合类型
 * @date 2024/1/8 - 10:12:50
 * @author Holy-Mortal
 *
 * @typedef {DemoExtract}
 */
type DemoExtract = Extract<"name" | "age" | "gender" | "exist", "name">;

/**
 * ? 实现过程
 * @typedef {CustomExtract}
 * @template T
 * @template U
 */
type CustomExtract<T, U> = T extends U ? T : never;

/**
 * 测试用例
 */
type ExtractTest001 = CustomExtract<
  "name" | "age" | "gender" | "exist",
  "name"
>;
