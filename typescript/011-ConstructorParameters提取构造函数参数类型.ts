/**
 * todo: ConstructorParameters<T extends abstract new (...args: any) => any>
 * 用于获取构造函数的参数类型
 * 接受一个构造函数类型作为参数，并返回一个元组类型，该元组类型包含了构造函数的参数类型
 * @date 2024/1/8 - 10:48:33
 * @author Holy-Mortal
 *
 * @typedef {DemoConstructorParameters}
 */
type DemoConstructorParameters = ConstructorParameters<
  typeof ConstructorParametersDemo
>;

/**
 * ? 实现过程
 * @typedef {CustomConstructorParameters}
 * @template {abstract new (...args: any) => any} T
 */
type CustomConstructorParameters<T extends abstract new (...args: any) => any> =
  T extends abstract new (...args: infer P) => any ? P : never;

/**
 * 测试用例
 */
class ConstructorParametersDemo {
  constructor(name: string, age: number) {
    // constructor implementation
  }
}
type ConstructorParametersTest001 = CustomConstructorParameters<
  typeof ConstructorParametersDemo
>;
