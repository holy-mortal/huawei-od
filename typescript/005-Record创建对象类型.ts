/**
 * todo: Record<K, T>
 * 用于创建一个具有指定属性类型的对象类型 K extends string | number | symbol
 * 接受两个类型参数，第一个参数指定属性的名称，第二个参数指定属性的类型
 * 可以很方便地定义一个具有指定属性类型的对象类型，这对于创建字典、映射等数据结构非常有用
 * @date 2024/1/8 - 10:00:37
 * @author Holy-Mortal
 *
 * @typedef {DemoRecord}
 */
type DemoRecord = Record<"name" | "prop", string>;

/**
 * ? 实现过程
 * @typedef {CustomRecord}
 * @template {string | number | symbol} K
 * @template T
 */
type CustomRecord<K extends string | number | symbol, T> = {
  [P in K]: T;
};

/**
 * 测试用例
 */
type RecordTest001 = CustomRecord<"name" | "prop", string>;
