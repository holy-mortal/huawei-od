/**
 * todo: InstanceType<T extends abstract new (...args: any) => any>
 * 用于获取构造函数的实例类型
 * 接受一个构造函数类型作为参数，并返回该构造函数类型的实例类型
 * @date 2024/1/8 - 11:02:11
 * @author Holy-Mortal
 *
 * @typedef {DemoInstanceType}
 */
type DemoInstanceType = InstanceType<typeof InstanceTypeDemo>;

/**
 * ? 实现过程
 * @typedef {CustomInstanceType}
 * @template {abstract new (...args: any) => any} T
 */
type CustomInstanceType<T extends abstract new (...args: any) => any> =
  T extends abstract new (...args: any) => infer R ? R : never;

/**
 * 测试用例
 */
class InstanceTypeDemo {
  name: string;
  age: number;
  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
  }
  sayHello() {
    console.log("InstanceType");
  }
}
type InstanceTypeTest001 = CustomInstanceType<typeof InstanceTypeDemo>;

const instance: DemoInstanceType = new InstanceTypeDemo("mortal", 27);
