/**
 * todo: NonNullable<T>
 * 用于从一个类型中排除 null 和 undefined
 * 接受一个类型参数，该参数表示要排除 null 和 undefined 的类型
 * @date 2024/1/8 - 10:23:26
 * @author Holy-Mortal
 *
 * @typedef {DemoNonNullable}
 */
type DemoNonNullable = NonNullable<"string" | null | undefined>;

/**
 * ? 实现过程
 * @typedef {CustomNonNullable}
 * @template T
 */
type CustomNonNullable<T> = T & {};

// type CustomNonNullable<T> = T extends null | undefined ? never : T

/**
 * 测试用例
 */
type NonNullableTest001 = CustomNonNullable<"string" | null | undefined>;
