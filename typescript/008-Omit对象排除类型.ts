/**
 * todo: Omit<T, K extends keyof T>
 * 用于从一个对象类型中排除指定的属性
 * 接受两个类型参数，第一个参数是要从中排除属性的对象类型，第二个参数是要排除的属性的名称
 * @date 2024/1/8 - 10:17:28
 * @author Holy-Mortal
 *
 * @typedef {DemoOmit}
 */
type DemoOmit = Omit<OmitDemo, "name">;

/**
 * ? 实现过程
 * @typedef {CustomOmit}
 * @template T
 * @template {keyof T} K
 */
type CustomOmit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

// type CustomOmit<T, K extends string | number | symbol> = {
//   [P in Exclude<keyof T, K>]: T[P];
// };

// type CustomOmit<T, K extends keyof T> = {
//   [P in keyof T as P extends K ? never : P]: T[P];
// };

/**
 * 测试用例
 */
type OmitDemo = {
  name: string;
  gender: string;
  age: number;
  exist: boolean;
};
type OmitTest001 = CustomOmit<OmitDemo, "name">;
