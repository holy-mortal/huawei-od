/**
 * todo Partial<T>
 * 用于将给定类型的所有属性设置为可选
 * 创建一个新的类型，该类型具有与原始类型相同的属性，但是这些属性都是可选的
 * @date 2024/1/8 - 09:12:55
 * @author Holy-Mortal
 *
 * @typedef {DemoPartial}
 */
type DemoPartial = Partial<PartialDemo>;

/**
 * ? 实现过程
 * @typedef {CustomPartial}
 * @template T
 */
type CustomPartial<T> = {
  [P in keyof T]?: T[P];
};

/**
 * 测试用例
 */
type PartialDemo = {
  name: string;
  gender: string;
  age: number;
  exist: boolean;
};
type PartialTest001 = CustomPartial<PartialDemo>;
