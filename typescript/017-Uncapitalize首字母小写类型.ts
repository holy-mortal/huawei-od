/**
 * todo: Uncapitalize<S extends string>
 * 用于将字符串的第一个字符转换为小写
 * @date 2024/1/8 - 11:19:48
 * @author Holy-Mortal
 *
 * @typedef {DemoUncapitalize}
 */
type DemoUncapitalize = Uncapitalize<"Hello,Typescript">;

/**
 * ? 实现过程
 * @typedef {CustomUncapitalize}
 * @template {string} S
 */
type CustomUncapitalize<S extends string> = intrinsic;

/**
 * 测试用例
 */
type UncapitalizeTest001 = CustomUncapitalize<"Hello,Typescript">;
