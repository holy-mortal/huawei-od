/**
 * todo: Pick<T, K extends keyof T>
 * 用于从给定类型中选择指定的属性，并创建一个新的类型
 * 可以从一个对象类型中挑选出指定的属性，创建一个新的类型，该类型只包含指定的属性
 * @date 2024/1/8 - 09:39:12
 * @author Holy-Mortal
 *
 * @typedef {DemoPick}
 */
type DemoPick = Pick<PickDemo, "name" | "age">;

/**
 * ? 实现过程
 * @typedef {CustomPick}
 * @template T
 * @template {keyof T} K
 */
type CustomPick<T, K extends keyof T> = {
  [P in K]: T[P];
};

/**
 * 测试用例
 */
type PickDemo = {
  name: string;
  gender: string;
  age: number;
  exist: boolean;
};

type PickTest001 = CustomPick<PickDemo, "name" | "age">;
