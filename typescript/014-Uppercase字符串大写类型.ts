/**
 * todo: Uppercase<S extends string>
 * 用于将字符串类型的字母转换为大写
 * 接受一个字符串类型作为参数，并返回该字符串类型的大写版本
 * @date 2024/1/8 - 11:09:23
 * @author Holy-Mortal
 *
 * @typedef {DemoUppercase}
 */
type DemoUppercase = Uppercase<"Hello,Typescript">;

/**
 * ? 实现过程
 * @typedef {CustomUppercase}
 * @template {string} S
 */
type CustomUppercase<S extends string> = intrinsic;

/**
 * 测试用例
 */
type UppercaseTest001 = CustomUppercase<"Hello,Typescript">;
