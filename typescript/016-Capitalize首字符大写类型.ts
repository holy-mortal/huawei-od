/**
 * todo: Capitalize<S extends string>
 * 用于将字符串的第一个字符转换为大写
 * @date 2024/1/8 - 11:16:22
 * @author Holy-Mortal
 *
 * @typedef {DemoCapitalize}
 */
type DemoCapitalize = Capitalize<"hello,Typescript">;

/**
 * ? 实现过程
 * @typedef {CustomCapitalize}
 * @template {string} S
 */
type CustomCapitalize<S extends string> = intrinsic;

/**
 * 测试用例
 */
type CapitalizeTest001 = CustomCapitalize<"hello,Typescript">;
