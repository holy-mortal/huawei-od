/**
 * todo: Parameters<T extends (...args: any) => any>
 * 一个泛型工具类型，它用于获取函数类型 T 的参数类型
 * 接受一个函数类型作为参数，并返回一个元组类型，其中包含了函数的每个参数类型
 * @date 2024/1/8 - 10:42:29
 * @author Holy-Mortal
 *
 * @typedef {DemoParameters}
 */
type DemoParameters = Parameters<typeof ParametersDemo>;

// infer 是一个关键字，用于在条件类型中推断类型变量
// 使用infer关键字将类型变量绑定到待推断的类型上。这样，在使用条件类型时，可以通过infer关键字来提取和操作这个类型
type CustomParameters<T extends (...args: any) => any> = T extends (
  ...args: infer P
) => any
  ? P
  : never;

/**
 * 测试用例
 */
function ParametersDemo(name: string, age: number): void {
  console.log("Parameters");
}
type ParametersTest001 = CustomParameters<typeof ParametersDemo>;
